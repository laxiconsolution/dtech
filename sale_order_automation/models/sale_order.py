from odoo import api, fields, models, _


class SaleOrder(models.Model):
    _inherit = "sale.order"

    invoice_state = fields.Boolean('Invoice State', compute="get_invoice_state")

    def get_invoice_state(self):
        for res in self:
            if all([inv.state == 'paid' for inv in res.invoice_ids]):
                res.invoice_state = True
            else:
                res.invoice_state = False

    def action_invoice_register_payment(self):
        for res in self.invoice_ids.filtered(lambda x: x.state != 'paid'):
            if res.state == 'draft':
                res.action_invoice_open()
            ctx = {
                'active_ids': res.ids,
                'active_model': 'account.invoice',
                'active_id': res.id,
                'default_type': "out_invoice",
                'default_partner_id': res.partner_id.id,
                'default_partner_shipping_id': res.partner_id.id or False,
                'default_payment_term_id': res.payment_term_id.id or False,
                'default_origin': res.origin or False,
                'default_user_id': res.user_id.id
            }
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'account.payment',
                'view_mode': 'form',
                'view_type': 'form',
                'views': [(self.env.ref('account.view_account_payment_invoice_form').id, 'form')],
                'target': 'new',
                'context': ctx
            }

    @api.multi
    def action_confirm(self):
        imediate_obj = self.env['stock.immediate.transfer']
        res = super(SaleOrder, self).action_confirm()
        for order in self:

            warehouse = order.warehouse_id
            if warehouse.is_delivery_set_to_done and order.picking_ids:
                for picking in self.picking_ids:
                    picking.action_confirm()
                    picking.action_assign()

                    imediate_rec = imediate_obj.create(
                        {'pick_ids': [(4, order.picking_ids.id)]})
                    imediate_rec.process()
                    if picking.state != 'done':
                        for move in picking.move_ids_without_package:
                            move.quantity_done = move.product_uom_qty
                        picking.button_validate()

            self._cr.commit()

            if warehouse.create_invoice and not order.invoice_ids:
                order.action_invoice_create()

            if warehouse.validate_invoice and order.invoice_ids:
                for invoice in order.invoice_ids:
                    invoice.action_invoice_open()

        return res
