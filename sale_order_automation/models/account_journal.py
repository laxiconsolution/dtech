# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import ValidationError


class AccountJournal(models.Model):
    _inherit = "account.journal"

    is_gst_number = fields.Boolean(string="GSTIN / UIN")

    @api.onchange('is_gst_number', 'type')
    def onchange_is_gst_number(self):
        if self.type in ('sale', 'purchase') and self.is_gst_number:
            journal = self.search([('is_gst_number', '=', True), ('type', '=', self.type), ('id', '!=', self._origin.id)])
            if journal:
                raise ValidationError("You can select a single general for any type")


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.onchange('partner_id', 'journal_id')
    def onchange_partner_id_journal(self):
        for res in self:
            if res.partner_id.gst_number:
                journal = self.env['account.journal'].search([('is_gst_number', '=', True)])
                for rec in journal:
                    if rec.type == 'sale' and res.type == 'out_invoice':
                        res.journal_id = rec.id
                    elif rec.type == 'purchase' and res.type == 'in_invoice':
                        res.journal_id = rec.id
