# -*- coding: utf-8 -*-
# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################


{
    'name': 'Barcode scanning support for sale and Purchase',
    'version': '12.0.1.0.1',
    'category': 'Sales',
    'summary': 'This module will help you to use barcode scanner in sales and purchase.',
    'author': 'Laxicon Solutions',
    'company': 'Laxicon Solutions',
    'website': 'https://www.laxicon.in',
    'depends': ['purchase', 'sale_management', 'barcodes'],
    'demo': [],
    'data': [
        'views/import_libraties.xml',
        'views/sale_order_line.xml',
        'views/purchase_order_line.xml',
    ],
    'installable': True,
    'application': False,
    'auto_install': False,
    'qweb': [],
    'license': 'AGPL-3',
}
