# -*- coding: utf-8 -*-

from odoo import models, _


class SaleOrder(models.Model):
    _name = "sale.order"
    _inherit = ['sale.order', 'barcodes.barcode_events_mixin']

    def _add_product(self, product):
        sol_id = self.env['sale.order.line'].search([('product_id', '=', product.id),
                                                     ('order_id', '=', self.id and self.id or self._origin.id)], limit=1)
        if sol_id:
            sol_id.product_uom_qty += 1
        else:
            data = {
                'product_id': product.id,
                'name': product.name,
                'order_id': self._origin.id
                }
            line_obj = self.env['sale.order.line']
            new_line_id = line_obj.new(data)
            new_line_id.product_id_change()
            new_line_id.product_uom_qty = 1
            self.order_line = [new_line_id.id]

    def on_barcode_scanned(self, barcode):
        product = self.env['product.template'].search([('barcode', '=', barcode)])
        if product:
            self._add_product(product)
            return
        else:
            warning = {
                    'title': _('Warning!'),
                    'message': _('Your Scanned Product Is Not Available.'),
                }
            return {'warning': warning}
