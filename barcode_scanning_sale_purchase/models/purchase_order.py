# -*- coding: utf-8 -*-

from odoo import models, _


class PurchaseOrder(models.Model):
    _name = "purchase.order"
    _inherit = ['purchase.order', 'barcodes.barcode_events_mixin']

    def _add_product(self, product):
        pol_id = self.env['purchase.order.line'].search([('product_id', '=', product.id),
                                                         ('order_id', '=', self.id and self.id or self._origin.id)], limit=1)
        if pol_id:
            pol_id.product_qty += 1
        else:
            data = {
                'product_id': product.id,
                'name': product.name,
                'order_id': self._origin.id
                }
            line_obj = self.env['purchase.order.line']
            new_line_id = line_obj.new(data)
            new_line_id.onchange_product_id()
            new_line_id.product_qty = 1
            self.order_line = [new_line_id.id]

    def on_barcode_scanned(self, barcode):
        product = self.env['product.template'].search([('barcode', '=', barcode)])
        if product:
            self._add_product(product)
            return
        else:
            warning = {
                    'title': _('Warning!'),
                    'message': _('Your Scanned Product Is Not Available.'),
                }
            return {'warning': warning}
