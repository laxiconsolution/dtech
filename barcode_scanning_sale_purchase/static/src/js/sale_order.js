odoo.define('barcode_scanning_sale_purchase.SaleOrder', function (require) {
"use strict";

var field_registry = require('web.field_registry');
var AbstractField = require('web.AbstractField');

var SaleOrder = AbstractField.extend({
    init: function() {
        this._super.apply(this, arguments);

        this.trigger_up('activeBarcode', {
            name: this.name,
            notifyChange: false,
            fieldName: 'order_line',
            quantity: 'product_uom_qty',
            setQuantityWithKeypress: true,
            commands: {
                barcode: '_barcodeAddX2MQuantity',
            }
        });
    },
});

field_registry.add('sale_order_scan', SaleOrder);

});
