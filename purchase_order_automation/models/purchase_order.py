from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    invoice_state = fields.Boolean('Invoice State', compute="get_invoice_state")

    @api.model
    def create(self, vals):
        if 'partner_ref' in vals and vals['partner_ref']:
            vals['partner_ref'] = vals['partner_ref'].strip()
            po = self.search([('partner_ref', '=', vals['partner_ref'])])
            if po:
                raise ValidationError(_("Purchase Order with %s vendor refernce already exist!" % (vals['partner_ref'])))
        return super(PurchaseOrder, self).create(vals)

    @api.multi
    def write(self, vals):
        if 'partner_ref' in vals and vals['partner_ref']:
            vals['partner_ref'] = vals['partner_ref'].strip()
            po = self.search([('partner_ref', '=', vals['partner_ref'])])
            if po:
                raise ValidationError(_("Purchase Order with %s vendor refernce already exist!" % (vals['partner_ref'])))
        return super(PurchaseOrder, self).write(vals)

    def get_invoice_state(self):
        for res in self:
            if all([inv.state == 'paid' for inv in res.invoice_ids]):
                res.invoice_state = True
            else:
                res.invoice_state = False

    def action_invoice_register_payment(self):
        for res in self.invoice_ids.filtered(lambda x: x.state != 'paid'):
            if res.state == 'draft':
                res.action_invoice_open()
            ctx = {
                'active_ids': res.ids,
                'active_model': 'account.invoice',
                'active_id': res.id,
                'default_type': "out_invoice",
                'default_partner_id': res.partner_id.id,
                'default_partner_shipping_id': res.partner_id.id or False,
                'default_payment_term_id': res.payment_term_id.id or False,
                'default_origin': res.origin or False,
                'default_user_id': res.user_id.id
            }
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'account.payment',
                'view_mode': 'form',
                'view_type': 'form',
                'views': [(self.env.ref('account.view_account_payment_invoice_form').id, 'form')],
                'target': 'new',
                'context': ctx
            }

    @api.multi
    def button_confirm(self):
        imediate_obj = self.env['stock.immediate.transfer']
        invoice_obj = self.env['account.invoice']
        res = super(PurchaseOrder, self).button_confirm()
        for order in self:

            company_id = order.company_id
            if company_id.is_po_delivery_set_to_done and order.picking_ids:
                for picking in self.picking_ids:
                    picking.action_confirm()
                    picking.action_assign()
                    imediate_rec = imediate_obj.create(
                        {'pick_ids': [(4, picking.id)]})
                    imediate_rec.process()

            if company_id.create_invoice_for_po and not order.invoice_ids:
                invoice_id = invoice_obj.create({'purchase_id': order.id, 'partner_id': order.partner_id.id,
                                                 'default_type': 'in_invoice', 'type': 'in_invoice',
                                                 'journal_type': 'purchase', 'account_id': order.partner_id.property_account_payable_id.id})
                invoice_id.purchase_order_change()
                invoice_id._onchange_partner_id()
                invoice_id._onchange_invoice_line_ids()
            if company_id.validate_po_invoice and order.invoice_ids:
                for invoice in order.invoice_ids:
                    invoice.action_invoice_open()

        return res
