# -*- coding: utf-8 -*-
# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountInvoiceLine(models.Model):
    _inherit = "account.invoice.line"

    gst_id = fields.Many2one('product.gst', string='HSN/Sac')

    @api.multi
    @api.onchange('product_id')
    def _onchange_product_id(self):
        res = super(AccountInvoiceLine, self)._onchange_product_id()
        gst_ids = {'domain': {'gst_id': [('id', 'in', self.product_id.categ_id.gst_ids.ids)]}}
        if self.product_id.is_gst:
            if self.product_id.gst_id:
                self.gst_id = self.product_id.gst_id
            if not self.gst_id:
                if not isinstance(res, dict):
                    res.update(gst_ids)
                else:
                    if not res.get('domain', {}):
                        res.update({'domain': {}})
                    res['domain'].update({'gst_id': [('id', 'in', self.product_id.categ_id.gst_ids.ids)]})
        else:
            self.gst_id = {}
        return res

    def _set_gst_taxes(self):
        user_state_id = self.env.user.company_id.state_id
        if not self.invoice_id.partner_id:
            raise UserError(_("Please add the Customer"))
        customer_state_id = self.invoice_id.partner_id.state_id
        if self.product_id.gst_id and self.invoice_id.partner_id.supplier:
            if customer_state_id == user_state_id:
                self.invoice_line_tax_ids += self.gst_id.cgst_purchase_tax_id + self.gst_id.sgst_purchase_tax_id
            else:
                self.invoice_line_tax_ids += self.gst_id.igst_purchase_tax_id
        elif self.product_id.gst_id and self.invoice_id.partner_id.customer:
            if customer_state_id == user_state_id:
                self.invoice_line_tax_ids += self.gst_id.cgst_sale_tax_id + self.gst_id.sgst_sale_tax_id
            else:
                self.invoice_line_tax_ids += self.gst_id.igst_sale_tax_id

    def _set_taxes(self):
        res = super(AccountInvoiceLine, self)._set_taxes()
        self._set_gst_taxes()
        return res

    @api.onchange('gst_id')
    def gst_id_change(self):
        self._set_gst_taxes()
