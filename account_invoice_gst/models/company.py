# -*- coding: utf-8 -*-
# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
from odoo import models, fields, api, _
from odoo.exceptions import UserError
import re


class ResCompany(models.Model):
    _inherit = 'res.company'

    pan_number = fields.Char(string='PAN')
    gst_number = fields.Char(string='GSTIN')
    gst_user = fields.Char(string='GST Username')
    gst_pass = fields.Char(string='GST Password')

    @api.model
    def default_get(self, fields):
        res = super(ResCompany, self).default_get(fields)
        if not res.get('country_id'):
            country_id = self.env.ref('base.in')
            res.update({'country_id': country_id.id})
        if not res.get('state_id'):
            state_id = self.env.ref('base.state_in_mh')
            res.update({'state_id': state_id.id})
        return res

    @api.one
    @api.constrains('gst_number', 'state_id')
    def _check_gst_number(self):
        # if not self.state_id:
        #     raise UserError(_("Please Add the State in the Address"))
        if self.gst_number and self.state_id:
            user_gst_state_code = str(self.gst_number)[:2]
            gst_state_code = self.state_id.gst_state_code
            if user_gst_state_code != gst_state_code:
                raise UserError(_("Your GST code is not matching with the State!"))
            if not(re.match("\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d[Z]{1}[A-Z\d]{1}", self.gst_number.upper())):
                raise UserError(_("Invalid GSTIN format.\r\n.GSTIN must be in the format nnAAAAAnnnnA_Z_ where n=number, A=alphabet, _=either."))

    @api.model
    def create(self, vals):
        res = super(ResCompany, self).create(vals)
        # sale tax for GST
        if res.id:
            igst_sale_5 = self.env.ref('account_invoice_gst.igst_sale_5')
            name = igst_sale_5.name.replace('(Copy)', '')
            new_igst_sale_5 = igst_sale_5.sudo().copy(default={'company_id': res.id})
            new_igst_sale_5.sudo().write({'name': name})

            sgst_sale_5 = self.env.ref('account_invoice_gst.sgst_sale_5')
            name = sgst_sale_5.name.replace('(Copy)', '')
            new_sgst_sale_5 = sgst_sale_5.sudo().copy(default={'company_id': res.id})
            new_sgst_sale_5.sudo().write({'name': name})

            cgst_sale_5 = self.env.ref('account_invoice_gst.cgst_sale_5')
            name = cgst_sale_5.name.replace('(Copy)', '')
            new_cgst_sale_5 = cgst_sale_5.sudo().copy(default={'company_id': res.id})
            new_cgst_sale_5.sudo().write({'name': name})

            igst_sale_12 = self.env.ref('account_invoice_gst.igst_sale_12')
            name = igst_sale_12.name.replace('(Copy)', '')
            new_igst_sale_12 = igst_sale_12.sudo().copy(default={'company_id': res.id})
            new_igst_sale_12.sudo().write({'name': name})

            sgst_sale_12 = self.env.ref('account_invoice_gst.sgst_sale_12')
            name = sgst_sale_12.name.replace('(Copy)', '')
            new_sgst_sale_12 = sgst_sale_12.sudo().copy(default={'company_id': res.id})
            new_sgst_sale_12.sudo().write({'name': name})

            cgst_sale_12 = self.env.ref('account_invoice_gst.cgst_sale_12')
            name = cgst_sale_12.name.replace('(Copy)', '')
            new_cgst_sale_12 = cgst_sale_12.sudo().copy(default={'company_id': res.id})
            new_cgst_sale_12.sudo().write({'name': name})

            igst_sale_18 = self.env.ref('account_invoice_gst.igst_sale_18')
            name = igst_sale_18.name.replace('(Copy)', '')
            new_igst_sale_18 = igst_sale_18.sudo().copy(default={'company_id': res.id})
            new_igst_sale_18.sudo().write({'name': name})

            sgst_sale_18 = self.env.ref('account_invoice_gst.sgst_sale_18')
            name = sgst_sale_18.name.replace('(Copy)', '')
            new_sgst_sale_18 = sgst_sale_18.sudo().copy(default={'company_id': res.id})
            new_sgst_sale_18.sudo().write({'name': name})

            cgst_sale_18 = self.env.ref('account_invoice_gst.cgst_sale_18')
            name = cgst_sale_18.name.replace('(Copy)', '')
            new_cgst_sale_18 = cgst_sale_18.sudo().copy(default={'company_id': res.id})
            new_cgst_sale_18.sudo().write({'name': name})

            igst_sale_28 = self.env.ref('account_invoice_gst.igst_sale_28')
            name = igst_sale_28.name.replace('(Copy)', '')
            new_igst_sale_28 = igst_sale_28.sudo().copy(default={'company_id': res.id})
            new_igst_sale_28.sudo().write({'name': name})

            sgst_sale_28 = self.env.ref('account_invoice_gst.sgst_sale_28')
            name = sgst_sale_28.name.replace('(Copy)', '')
            new_sgst_sale_28 = sgst_sale_28.sudo().copy(default={'company_id': res.id})
            new_sgst_sale_28.sudo().write({'name': name})

            cgst_sale_28 = self.env.ref('account_invoice_gst.cgst_sale_28')
            name = cgst_sale_28.name.replace('(Copy)', '')
            new_cgst_sale_28 = cgst_sale_28.sudo().copy(default={'company_id': res.id})
            new_cgst_sale_28.sudo().write({'name': name})

            # purchase tax for gst
            igst_purchase_5 = self.env.ref('account_invoice_gst.igst_purchase_5')
            name = igst_purchase_5.name.replace('(Copy)', '')
            new_igst_purchase_5 = igst_purchase_5.sudo().copy(default={'company_id': res.id})
            new_igst_purchase_5.sudo().write({'name': name})

            sgst_purchase_5 = self.env.ref('account_invoice_gst.sgst_purchase_5')
            name = sgst_purchase_5.name.replace('(Copy)', '')
            new_sgst_purchase_5 = sgst_purchase_5.sudo().copy(default={'company_id': res.id})
            new_sgst_purchase_5.sudo().write({'name': name})

            cgst_purchase_5 = self.env.ref('account_invoice_gst.cgst_purchase_5')
            name = cgst_purchase_5.name.replace('(Copy)', '')
            new_cgst_purchase_5 = cgst_purchase_5.sudo().copy(default={'company_id': res.id})
            new_cgst_purchase_5.sudo().write({'name': name})

            igst_purchase_12 = self.env.ref('account_invoice_gst.igst_purchase_12')
            name = igst_purchase_12.name.replace('(Copy)', '')
            new_igst_purchase_12 = igst_purchase_12.sudo().copy(default={'company_id': res.id})
            new_igst_purchase_12.sudo().write({'name': name})

            sgst_purchase_12 = self.env.ref('account_invoice_gst.sgst_purchase_12')
            name = sgst_purchase_12.name.replace('(Copy)', '')
            new_sgst_purchase_12 = sgst_purchase_12.sudo().copy(default={'company_id': res.id})
            new_sgst_purchase_12.sudo().write({'name': name})

            cgst_purchase_12 = self.env.ref('account_invoice_gst.cgst_purchase_12')
            name = cgst_purchase_12.name.replace('(Copy)', '')
            new_cgst_purchase_12 = cgst_purchase_12.sudo().copy(default={'company_id': res.id})
            new_cgst_purchase_12.sudo().write({'name': name})

            igst_purchase_18 = self.env.ref('account_invoice_gst.igst_purchase_18')
            name = igst_purchase_18.name.replace('(Copy)', '')
            new_igst_purchase_18 = igst_purchase_18.sudo().copy(default={'company_id': res.id})
            new_igst_purchase_18.sudo().write({'name': name})

            sgst_purchase_18 = self.env.ref('account_invoice_gst.sgst_purchase_18')
            name = sgst_purchase_18.name.replace('(Copy)', '')
            new_sgst_purchase_18 = sgst_purchase_18.sudo().copy(default={'company_id': res.id})
            new_sgst_purchase_18.sudo().write({'name': name})

            cgst_purchase_18 = self.env.ref('account_invoice_gst.cgst_purchase_18')
            name = cgst_purchase_18.name.replace('(Copy)', '')
            new_cgst_purchase_18 = cgst_purchase_18.sudo().copy(default={'company_id': res.id})
            new_cgst_purchase_18.sudo().write({'name': name})

            igst_purchase_28 = self.env.ref('account_invoice_gst.igst_purchase_28')
            name = igst_purchase_28.name.replace('(Copy)', '')
            new_igst_purchase_28 = igst_purchase_28.sudo().copy(default={'company_id': res.id})
            new_igst_purchase_28.sudo().write({'name': name})

            sgst_purchase_28 = self.env.ref('account_invoice_gst.sgst_purchase_28')
            name = sgst_purchase_28.name.replace('(Copy)', '')
            new_sgst_purchase_28 = sgst_purchase_28.sudo().copy(default={'company_id': res.id})
            new_sgst_purchase_28.sudo().write({'name': name})

            cgst_purchase_28 = self.env.ref('account_invoice_gst.cgst_purchase_28')
            name = cgst_purchase_28.name.replace('(Copy)', '')
            new_cgst_purchase_28 = cgst_purchase_28.sudo().copy(default={'company_id': res.id})
            new_cgst_purchase_28.sudo().write({'name': name})
        return res
