# -*- coding: utf-8 -*-
# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import re


class ResPartner(models.Model):
    _inherit = 'res.partner'

    gst_number = fields.Char(string='GSTIN/ UIN')
    gst_res_type = fields.Selection([('regular', 'Regular'), ('unregister', 'Unregister'), ('composition', 'Composition'), ('consumer', 'Consumer')], string="Registration Type", default="regular")
    party_type = fields.Selection([('not_applicable', 'Not Applicable'), ('deemed_export', 'Deemed Export'), ('embassy_un_body', 'Embassy /UN Body'), ('sez', 'SEZ')], string="Party Type", default="not_applicable")

    @api.model
    def default_get(self, fields):
        res = super(ResPartner, self).default_get(fields)
        if not res.get('country_id'):
            country_id = self.env.ref('base.in')
            res.update({'country_id': country_id.id})
        if not res.get('state_id'):
            state_id = self.env.ref('base.state_in_gj')
            res.update({'state_id': state_id.id})
        return res

    # @api.model
    # def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
    #     res = super(ResPartner, self).fields_view_get(
    #            view_id=view_id, view_type=view_type, toolbar=toolbar,
    #            submenu=submenu)
    #     doc = etree.XML(res['arch'])
    #     nodes = doc.xpath("//field[@name='zip']")
    #     for node in nodes:
    #         node.set('placeholder', 'Pincode')
    #         setup_modifiers(node, res['fields']['zip'])
    #     nodes = doc.xpath("//field[@name='website']")
    #     for node in nodes:
    #         node.set('placeholder', 'e.g. www.mygstbooks.com')
    #         setup_modifiers(node, res['fields']['website'])
    #     res['arch'] = etree.tostring(doc)
    #     return res

    @api.onchange('state_id')
    def onchange_state_id(self):
        self.country_id = self.state_id.country_id

    @api.one
    @api.constrains('gst_number', 'state_id')
    def _check_gst_number(self):
        # if self.gst_number:
        # pattern = "[0-9]{2}[a-zA-Z]{5}[0-9]{4}[a-zA-Z]{1}[1-9A-Za-z]{1}[C]{1}[0-9a-zA-Z]{1}"
        # if not self.state_id:
        #     raise UserError(_("Please Add the State in the Address"))
        if self.gst_number and self.state_id:
            user_gst_state_code = str(self.gst_number)[:2]
            gst_state_code = self.state_id.gst_state_code
            if user_gst_state_code != gst_state_code:
                raise UserError(_("Your GST code is not matching with the State!"))
        if self.gst_number:
            if not(re.match("\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d[Z]{1}[A-Z\d]{1}", self.gst_number.upper())):
                raise UserError(_("Invalid GSTIN format.\r\n.GSTIN must be in the format nnAAAAAnnnnA_Z_ where n=number, A=alphabet, _=either."))

    @api.onchange('gst_number', 'state_id')
    def onchange_gst_state(self):
        if self.gst_number and self.state_id:
            user_gst_state_code = str(self.gst_number)[:2]
            gst_state_code = self.state_id.gst_state_code
            if user_gst_state_code != gst_state_code:
                raise UserError(_("Your GST code is not matching with the State!"))
        if self.gst_number:
            if len(self.gst_number) != 15:
                return {
                    'warning': {'title': 'Warning', 'message': 'Invalid GSTIN. GSTIN number must be 15 digits. Please check.'},
                }
            if not(re.match("\d{2}[A-Z]{5}\d{4}[A-Z]{1}\d[Z]{1}[A-Z\d]{1}", self.gst_number.upper())):
                return {
                    'warning': {'title': 'Warning', 'message': 'Invalid GSTIN format.\r\n.GSTIN must be in the format nnAAAAAnnnnA_Z_ where n=number, A=alphabet, _=either.'},
                }
