# -*- coding: utf-8 -*-
# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
from odoo import api, fields, models


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.depends('partner_id', 'invoice_line_ids.price_subtotal', 'tax_line_ids.amount')
    def get_tax_total(self):
        for res in self:
            tax_total = {}
            for rec in res.tax_line_ids:
                tax_type = rec.tax_id.tax_type and rec.tax_id.tax_type or 'other'
                if tax_type not in tax_total:
                    tax_total.update({
                        tax_type: rec.amount
                    })
                else:
                    tax_total[tax_type] += rec.amount
        return tax_total

    partner_gst = fields.Char(string="GSTIN/UIN", readonly=True, states={'draft': [('readonly', False)]})
    e_commerce_gst = fields.Char(string="E-commerce GSTIN", readonly=True, states={'draft': [('readonly', False)]})
    reversed_charged = fields.Selection([('y', 'Yes'), ('n', 'No')], string="Reverse Charge(Y/N)", default="n", readonly=True, states={'draft': [('readonly', False)]})
    is_bill_of_supply = fields.Selection([('y', 'Yes'), ('n', 'No')], string="Is Bill of Supply(Y/N)", default="n", readonly=True, states={'draft': [('readonly', False)]})

    cgst_tax = fields.Float(compute="get_tax_total", string="CGST", store=True)
    sgst_tax = fields.Float(compute="get_tax_total", string="SGST", store=True)
    igst_tax = fields.Float(compute="get_tax_total", string="IGST", store=True)
    cess_tax = fields.Float(compute="get_tax_total", string="Cess", store=True)
    other_tax = fields.Float(compute="get_tax_total", string="Other", store=True)

    @api.onchange('partner_id')
    def onchange_partner(self):
        self.partner_gst = ''
        if self.partner_id and self.partner_id.gst_number:
            self.partner_gst = self.partner_id.gst_number

    @api.multi
    def number_to_words(self, n):
        words = ''

        units = ['', 'One', 'Two', 'Three', 'Four', 'Five',
                 'Six', 'Seven', 'Eight', 'Nine', 'Ten', 'Eleven',
                 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen',
                 'Seventeen', 'Eighteen', 'Nineteen']
        tens = ['', 'Ten', 'Twenty', 'Thirty', 'Forty', 'Fifty',
                'Sixty', 'Seventy', 'Eighty', 'Ninety']

        for group in ['', 'hundred', 'thousand', 'lac', 'crore']:

            if group in ['', 'thousand', 'lac']:
                n, digits = n // 100, n % 100
            elif group == 'hundred':
                n, digits = n // 10, n % 10
            else:
                digits = n

            if digits in range(1, 20):
                words = units[digits] + ' ' + group + ' ' + words
            elif digits in range(20, 100):
                ten_digit, unit_digit = digits // 10, digits % 10
                words = tens[ten_digit] + ' ' + units[unit_digit] + ' ' + group + ' ' + words
            elif digits >= 100:
                words = self.number_to_words(digits) + ' crore ' + words
        words = words + ' Only'
        return words

    def get_type(self):
        tax_type = []
        for rec in self.tax_line_ids:
            if rec.tax_id.tax_type:
                if rec.tax_id.tax_type not in tax_type:
                    tax_type.append(str(rec.tax_id.tax_type))
            elif 'other' not in tax_type:
                tax_type.append('other')
        return tax_type

    def amount(self, line):
        total_amt = 0.0
        for rec in line:
            total_amt = (rec.price_unit * rec.quantity)
        return total_amt

    def get_line_total(self, line):
        price_total = 0.0
        for rec in line:
            price_total = line.price_subtotal
            for tax in rec.invoice_line_tax_ids:
                price_total = price_total + ((line.price_subtotal * tax.amount) / 100.0)
        return round(price_total, 2)

    def get_line_tax(self, line):
        invoice = line.invoice_id
        price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)

        taxes = line.invoice_line_tax_ids.compute_all(price_unit,
                                                      invoice.currency_id,
                                                      line.quantity,
                                                      line.product_id,
                                                      invoice.partner_id)['taxes']
        tax_dict = {}
        for line in taxes:
            tax = self.env['account.tax'].sudo().browse(line['id'])
            tax_type = tax.tax_type and tax.tax_type or 'other'
            if tax_type not in tax_dict:
                tax_dict[tax_type] = {}
            tax_dict[tax_type].update({
                'rate': tax.amount,
                'amount': line['amount'],
                'tax_type': tax_type,
                'tax_id': tax,
            })
        return tax_dict

    def get_lines(self, order):
        lines_by_tax_id = {}
        for line in order.order_line:
            if line.invoice_line_tax_ids:
                if line.invoice_line_tax_ids not in lines_by_tax_id:
                    lines_by_tax_id.update({line.invoice_line_tax_ids: {'lines': [], 'sum': 0.0}})
                lines_by_tax_id[line.product_id.tax_id]['lines'].append(line)
                lines_by_tax_id[line.product_id.tax_id]['sum'] += line.price_subtotal
                self.total += line.price_subtotal
        return lines_by_tax_id

    def get_amount_in_word(self):
        val = self.sudo().number_to_words(int(self.amount_total))
        return val

    def total_disc(self):
        total_amt = 0.0
        final = 0.0
        for rec in self.invoice_line_ids:
            total_amt = (rec.price_unit * rec.quantity)
            final = final + total_amt
        return final

    def get_total(self):
        return self.total
