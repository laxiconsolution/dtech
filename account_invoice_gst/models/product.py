# -*- coding: utf-8 -*-
# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
from odoo import api, fields, models


class product_template(models.Model):
    _inherit = "product.template"

    is_gst = fields.Boolean('Is GST', default=False, help="is it include in gst or not")
    gst_id = fields.Many2one('product.gst', string='HSN Code')

    @api.onchange('is_gst')
    def is_gst_change(self):
        if not self.is_gst:
            self.gst_id = {}

    @api.model
    def default_get(self, fields):
        rec = super(product_template, self).default_get(fields)
        rec.update({'supplier_taxes_id': False, 'taxes_id': False})
        return rec
