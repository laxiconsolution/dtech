# -*- coding: utf-8 -*-
# -*- encoding: UTF-8 -*-
##############################################################################
#
#    Odoo, Open Source Management Solution
#    Copyright (C) 2015-Today Laxicon Solution.
#    (<http://laxicon.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
{
    'name': "GST Invoice",
    'summary': """GST Invoice""",
    'description': """
    GST Invoice for Accounting purpose and report

    Indian Accounting
    GST
    Indian GST
    GST Report
    GST Tax

    """,
    'author': "Laxicon Solution",
    'website': "www.laxicon.in",
    'price': 40.0,
    'sequence': 1,
    'currency': 'EUR',
    'category': 'Accounting & Finance',
    'version': '1.0',
    'depends': ['account', 'l10n_in', 'product'],
    'images': ['static/description/gst_logo.png'],
    'data': [
        # 'data/gst_update.xml',
        'data/gst_tax_list.xml',
        'data/gst_code_view.xml',
        'security/ir.model.access.csv',
        'security/security.xml',
        'views/product_view.xml',
        'views/product_gst_view.xml',
        'views/account_order_line_view.xml',
        'views/partner_view.xml',
        'views/company_view.xml',
        'views/res_state_view.xml',
        'views/account_tax_view.xml',
        'views/product_category_view.xml',
        'report/gst_invoice_reg.xml',
        'report/gst_tax_invoice_view.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
