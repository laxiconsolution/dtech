from odoo import api, fields, models, _

class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    # when cancel the po check if invoice available if yes then cancel the invoice.also check related picking..cancel the picking.
    @api.multi
    def button_cancel(self):
        for order in self:
            for inv in order.invoice_ids:
                if inv and inv.state in ('open', 'draft', 'paid'):
                    inv.sudo().action_cancel()#action_invoice_cancel()
            dict ={}
            pickings = self.env['stock.picking'].search([('group_id','=',order.group_id.id),('state', '!=', 'cancel')])
            pick = pickings.filtered(lambda x : x.picking_type_id.code == 'incoming')
            if pick:
                dict.update({1:pick})
                input_picking = pickings.filtered(lambda x :pick[0].location_dest_id.id == x.location_id.id)
                if input_picking:
                    dict.update({2:input_picking})
                    stock_picking = pickings.filtered(lambda x : input_picking[0].location_dest_id.id == x.location_id.id)
                    if stock_picking:
                        dict.update({3:stock_picking})
                if dict.get(3,False):
                    for cancel in dict.get(3):
                        if cancel.state != 'cancel':
                            cancel.with_context({'from_picking': True}).action_cancel()
                if dict.get(2,False):
                    for cancel in dict.get(2):
                        if cancel.state != 'cancel':
                            cancel.with_context({'from_picking': True}).action_cancel()
                if dict.get(1,False):
                    for cancel in dict.get(1):
                        if cancel.state != 'cancel':
                            cancel.with_context({'from_picking': True}).action_cancel()
            order.write({'state': 'cancel'})

        return super(PurchaseOrder, self).button_cancel()

    # set to purchase order in draft state and cancel the related invoice and picking.
    @api.multi
    def button_draft(self):
        self.button_cancel()
        return super(PurchaseOrder, self).button_draft()