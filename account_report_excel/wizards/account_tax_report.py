# -*- coding: utf-8 -*-
import base64
from io import BytesIO
import xlsxwriter
from odoo import api, models, _


class AccountTaxReport(models.TransientModel):
    _inherit = "account.common.report"
    _name = 'account.tax.report'
    _description = 'Tax Report'

    def _print_report(self, data):
        return self.env.ref('account_report_excel.action_report_account_tax').report_action(self, data=data)

    def _sql_from_amls_one(self):
        sql = """SELECT "account_move_line".tax_line_id, COALESCE(SUM("account_move_line".debit-"account_move_line".credit), 0)
                FROM %s
                WHERE %s AND "account_move_line".tax_exigible GROUP BY "account_move_line".tax_line_id"""
        return sql

    def _sql_from_amls_two(self):
        sql = """SELECT r.account_tax_id, COALESCE(SUM("account_move_line".debit-"account_move_line".credit), 0)
                 FROM %s
                 INNER JOIN account_move_line_account_tax_rel r ON ("account_move_line".id = r.account_move_line_id)
                 INNER JOIN account_tax t ON (r.account_tax_id = t.id)
                 WHERE %s AND "account_move_line".tax_exigible GROUP BY r.account_tax_id"""
        return sql

    def _compute_from_amls(self, options, taxes):
        # compute the tax amount
        sql = self._sql_from_amls_one()
        tables, where_clause, where_params = self.env['account.move.line']._query_get()
        query = sql % (tables, where_clause)
        self.env.cr.execute(query, where_params)
        results = self.env.cr.fetchall()
        for result in results:
            if result[0] in taxes:
                taxes[result[0]]['tax'] = abs(result[1])

        # compute the net amount
        sql2 = self._sql_from_amls_two()
        query = sql2 % (tables, where_clause)
        self.env.cr.execute(query, where_params)
        results = self.env.cr.fetchall()
        for result in results:
            if result[0] in taxes:
                taxes[result[0]]['net'] = abs(result[1])

    @api.model
    def get_lines(self, options):
        taxes = {}
        for tax in self.env['account.tax'].search([('type_tax_use', '!=', 'none')]):
            if tax.children_tax_ids:
                for child in tax.children_tax_ids:
                    if child.type_tax_use != 'none':
                        continue
                    taxes[child.id] = {'tax': 0, 'net': 0, 'name': child.name, 'type': tax.type_tax_use}
            else:
                taxes[tax.id] = {'tax': 0, 'net': 0, 'name': tax.name, 'type': tax.type_tax_use}
        self.with_context(date_from=options['date_from'], date_to=options['date_to'], strict_range=True)._compute_from_amls(options, taxes)
        groups = dict((tp, []) for tp in ['sale', 'purchase'])
        for tax in taxes.values():
            if tax['tax']:
                groups[tax['type']].append(tax)
        return groups

    @api.multi
    def check_report_excel(self):
        self.ensure_one()
        row_data = self.check_report()
        data = row_data.get('data', {})
        lines = self.get_lines(data.get('form'))
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        worksheet = workbook.add_worksheet(_('Tax Report'))
        style_bold_font = workbook.add_format({
            'valign': 'vjustify',
            'bold': True,
            'align': 'center',
            })
        style_font_right = workbook.add_format({
            'align': 'right'
            })
        row = 0
        col = 0
        worksheet.merge_range(row, col, row, col + 2, _('Tax Report'), style_bold_font)
        row += 2
        col = 0
        worksheet.write(row, col, _('Start Date'), style_bold_font)
        col += 2
        worksheet.write(row, col, _('End Date'), style_bold_font)
        row += 1
        col = 0
        worksheet.write(row, col, str(self.date_from) or '')
        col += 2
        worksheet.write(row, col, str(self.date_to) or '')
        row += 2
        col = 0
        worksheet.write(row, col, _('Sale'), style_bold_font)
        col += 1
        worksheet.write(row, col, _('Net'), style_bold_font)
        col += 1
        worksheet.write(row, col, _('Tax'), style_bold_font)
        row += 1
        for s_line in lines['sale']:
            col = 0
            worksheet.write(row, col, s_line.get('name'))
            col += 1
            worksheet.write(row, col, '%.2f' % s_line.get('net'), style_font_right)
            col += 1
            worksheet.write(row, col, '%.2f' % s_line.get('tax'), style_font_right)
            row += 1
        col = 0
        worksheet.write(row, col, _('Purchase'), style_bold_font)
        row += 1
        for s_line in lines['purchase']:
            col = 0
            worksheet.write(row, col, s_line.get('name'))
            col += 1
            worksheet.write(row, col, '%.2f' % s_line.get('net'), style_font_right)
            col += 1
            worksheet.write(row, col, '%.2f' % s_line.get('tax'), style_font_right)
            row += 1

        workbook.close()
        file_base = base64.b64encode(fp.getvalue())
        fp.close()
        wiz_id = self.env['sheets.excel.output'].create({'filename': file_base, 'name': 'Tax_Report.xls'})
        return {
           'type': 'ir.actions.act_window',
           'res_model': 'sheets.excel.output',
           'view_mode': 'form',
           'view_type': 'form',
           'res_id': wiz_id.id,
           'target': 'new',
        }
