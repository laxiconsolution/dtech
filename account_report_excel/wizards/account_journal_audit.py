# -*- coding: utf-8 -*-
import base64
from io import BytesIO
import xlsxwriter
from odoo import api, fields, models, _


class AccountPrintJournal(models.TransientModel):
    _inherit = "account.common.journal.report"
    _name = "account.print.journal"
    _description = "Account Print Journal"

    sort_selection = fields.Selection([('date', 'Date'), ('move_name', 'Journal Entry Number')], 'Entries Sorted by', required=True, default='move_name')
    journal_ids = fields.Many2many('account.journal', string='Journals', required=True, default=lambda self: self.env['account.journal'].search([('type', 'in', ['cash'])]))

    def _print_report(self, data):
        data = self.pre_print_report(data)
        data['form'].update({'sort_selection': self.sort_selection})
        return self.env.ref('account_report_excel.action_report_journal').with_context(landscape=True).report_action(self, data=data)

    def lines(self, target_move, journal_ids, sort_selection, data):
        if isinstance(journal_ids, int):
            journal_ids = [journal_ids]

        move_state = ['draft', 'posted']
        if target_move == 'posted':
            move_state = ['posted']

        query_get_clause = self._get_query_get_clause(data)
        params = [tuple(move_state), tuple(journal_ids)] + query_get_clause[2]
        query = 'SELECT "account_move_line".id FROM ' + query_get_clause[0] + ', account_move am, account_account acc WHERE "account_move_line".account_id = acc.id AND "account_move_line".move_id=am.id AND am.state IN %s AND "account_move_line".journal_id IN %s AND ' + query_get_clause[1] + ' ORDER BY '
        if sort_selection == 'date':
            query += '"account_move_line".date'
        else:
            query += 'am.name'
        query += ', "account_move_line".move_id, acc.code'
        self.env.cr.execute(query, tuple(params))
        ids = (x[0] for x in self.env.cr.fetchall())
        return self.env['account.move.line'].browse(ids)

    def _sum_debit(self, data, journal_id):
        move_state = ['draft', 'posted']
        if data['form'].get('target_move', 'all') == 'posted':
            move_state = ['posted']

        query_get_clause = self._get_query_get_clause(data)
        params = [tuple(move_state), tuple(journal_id.ids)] + query_get_clause[2]
        self.env.cr.execute('SELECT SUM(debit) FROM ' + query_get_clause[0] + ', account_move am '
                            'WHERE "account_move_line".move_id=am.id AND am.state IN %s AND "account_move_line".journal_id IN %s AND ' + query_get_clause[1] + ' ',
                            tuple(params))
        return self.env.cr.fetchone()[0] or 0.0

    def _sum_credit(self, data, journal_id):
        move_state = ['draft', 'posted']
        if data['form'].get('target_move', 'all') == 'posted':
            move_state = ['posted']

        query_get_clause = self._get_query_get_clause(data)
        params = [tuple(move_state), tuple(journal_id.ids)] + query_get_clause[2]
        self.env.cr.execute('SELECT SUM(credit) FROM ' + query_get_clause[0] + ', account_move am '
                            'WHERE "account_move_line".move_id=am.id AND am.state IN %s AND "account_move_line".journal_id IN %s AND ' + query_get_clause[1] + ' ',
                            tuple(params))
        return self.env.cr.fetchone()[0] or 0.0

    def _get_taxes(self, data, journal_id):
        move_state = ['draft', 'posted']
        if data['form'].get('target_move', 'all') == 'posted':
            move_state = ['posted']

        query_get_clause = self._get_query_get_clause(data)
        params = [tuple(move_state), tuple(journal_id.ids)] + query_get_clause[2]
        query = """
            SELECT rel.account_tax_id, SUM("account_move_line".balance) AS base_amount
            FROM account_move_line_account_tax_rel rel, """ + query_get_clause[0] + """
            LEFT JOIN account_move am ON "account_move_line".move_id = am.id
            WHERE "account_move_line".id = rel.account_move_line_id
                AND am.state IN %s
                AND "account_move_line".journal_id IN %s
                AND """ + query_get_clause[1] + """
           GROUP BY rel.account_tax_id"""
        self.env.cr.execute(query, tuple(params))
        ids = []
        base_amounts = {}
        for row in self.env.cr.fetchall():
            ids.append(row[0])
            base_amounts[row[0]] = row[1]
        res = {}
        for tax in self.env['account.tax'].browse(ids):
            self.env.cr.execute('SELECT sum(debit - credit) FROM ' + query_get_clause[0] + ', account_move am '
                                'WHERE "account_move_line".move_id=am.id AND am.state IN %s AND "account_move_line".journal_id IN %s AND ' + query_get_clause[1] + ' AND tax_line_id = %s',
                                tuple(params + [tax.id]))
            res[tax] = {
                'base_amount': base_amounts[tax.id],
                'tax_amount': self.env.cr.fetchone()[0] or 0.0,
            }
            if journal_id.type == 'sale':
                #  sales operation are credits
                res[tax]['base_amount'] = res[tax]['base_amount'] * -1
                res[tax]['tax_amount'] = res[tax]['tax_amount'] * -1
        return res

    def _get_query_get_clause(self, data):
        return self.env['account.move.line'].with_context(data['form'].get('used_context', {}))._query_get()

    @api.multi
    def check_report_excel(self):
        self.ensure_one()
        row_data = self.check_report()
        data = row_data.get('data', {})
        data = self.pre_print_report(data)
        journal_ids = self.env['account.journal'].browse(data['form']['journal_ids'])
        fp = BytesIO()
        workbook = xlsxwriter.Workbook(fp)
        for journal in journal_ids:
            worksheet = workbook.add_worksheet(_(journal.name + " Journal"))
            style_bold_font = workbook.add_format({
                'valign': 'vjustify',
                'bold': True,
                'align': 'center',
                })
            style_font_right = workbook.add_format({
                'align': 'right'
                })
            row = 0
            col = 0
            worksheet.merge_range(row, col, row, col + 6, _('Sale/Purchase Journal'), style_bold_font)
            row += 2
            col = 0
            worksheet.merge_range(row, col, row, col + 5, _(journal.name + " Journal"), style_bold_font)

            row += 2
            worksheet.merge_range(row, col, row, col + 1, _('Entries Sorted By'), style_bold_font)
            col += 2
            worksheet.merge_range(row, col, row, col + 1, _('Target Moves'), style_bold_font)
            col += 2
            worksheet.write(row, col, _('Start Date'), style_bold_font)
            col += 1
            worksheet.write(row, col, _(self.date_from) or '', style_bold_font)

            # 2nd row
            row += 1
            col = 0
            move = ''
            if self.target_move == 'draft':
                move = _('All Entries')
            if self.target_move == 'posted':
                move = _('All Posted Entries')
            worksheet.merge_range(row, col, row, col + 1, move, style_bold_font)
            sort = ''
            if self.sort_selection == 'date':
                sort = "Date"
            if self.sort_selection == 'move_name':
                sort = 'Journal Entry Number'

            col += 2
            worksheet.merge_range(row, col, row, col + 1, sort, style_bold_font)
            col += 2
            worksheet.write(row, col, _('End Date'), style_bold_font)
            col += 1
            worksheet.write(row, col, _(self.date_to) or '', style_bold_font)
            row += 2
            col = 0
            worksheet.write(row, col, _('Move'), style_bold_font)
            col += 1
            worksheet.write(row, col, _('Date'), style_bold_font)
            col += 1
            worksheet.write(row, col, _('Account'), style_bold_font)
            col += 1
            worksheet.write(row, col, _('Partner'), style_bold_font)
            col += 1
            worksheet.write(row, col, _('Label'), style_bold_font)
            col += 1
            worksheet.write(row, col, _('Debit'), style_bold_font)
            col += 1
            worksheet.write(row, col, _('Credit'), style_bold_font)
            journal_data = self.lines(self.target_move, journal.id, self.sort_selection, data)
            for jour in journal_data:
                row += 1
                col = 0
                worksheet.write(row, col, _(jour.move_id.name or str(jour.move_id.name)))
                col += 1
                worksheet.write(row, col, _(jour.date))
                col += 1
                worksheet.write(row, col, _(jour.account_id.code))
                col += 1
                worksheet.write(row, col, _(jour.sudo().partner_id and jour.sudo().partner_id.name and jour.sudo().partner_id.name[:23] or ''))
                col += 1
                worksheet.write(row, col, _(jour.name[:35]))
                col += 1
                worksheet.write(row, col, _("%.2f" % round(jour.debit, 2) or 0.0), style_font_right)
                col += 1
                worksheet.write(row, col, _("%.2f" % round(jour.credit, 2) or 0.0), style_font_right)
            row += 2
            col = 4
            worksheet.write(row, col, _("Total"), style_bold_font)
            col += 1
            worksheet.write(row, col, self._sum_debit(data, journal), style_bold_font)
            col += 1
            worksheet.write(row, col, self._sum_credit(data, journal), style_bold_font)

            row += 3
            col = 0
            worksheet.merge_range(row, col, row, col + 2, _('Tax Declaration'), style_bold_font)
            row += 1
            worksheet.write(row, col, _('Name'), style_bold_font)
            col += 1
            worksheet.write(row, col, _('Base Amount'), style_bold_font)
            col += 1
            worksheet.write(row, col, _('Tax Amount'), style_bold_font)
            row += 1
            all_tax = self._get_taxes(data, journal)
            for tax in all_tax:
                col = 0
                worksheet.write(row, col, _(tax.name))
                col += 1
                worksheet.write(row, col, all_tax.get(tax).get('base_amount'))
                col += 1
                worksheet.write(row, col, all_tax.get(tax).get('tax_amount'))
                row += 1

        workbook.close()
        file_base = base64.b64encode(fp.getvalue())
        fp.close()
        wiz_id = self.env['sheets.excel.output'].create({'filename': file_base, 'name': 'Journal_Audit.xls'})
        return {
           'type': 'ir.actions.act_window',
           'res_model': 'sheets.excel.output',
           'view_mode': 'form',
           'view_type': 'form',
           'res_id': wiz_id.id,
           'target': 'new',
        }
