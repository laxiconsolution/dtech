{
    'name': 'Sale Order Lot Selection',
    'version': '12.0.2.0.2',
    'category': 'Sales Management',
    'author': "Laxicon Solution",
    'website': 'https://www.Laxicon.in',
    'license': 'AGPL-3',
    'depends': ['sale_stock', 'sale_management'],
    'data': [
        'security/ir.model.access.csv',
        'view/sale_view.xml',
        'view/sale_lot_report.xml',
        'view/stock_move_views.xml',
        'wizard/sale_lot_report_wizard_view.xml',
    ],
    'demo': ['demo/sale_demo.xml'],
    'installable': True,
}
