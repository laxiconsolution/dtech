# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    lot_id = fields.Many2many(
        'stock.production.lot', string='Lot', copy=False)
    tracking = fields.Selection(related="product_id.tracking", string='Tracking')

    @api.multi
    @api.onchange('product_id')
    def product_id_change(self):
        super(SaleOrderLine, self).product_id_change()
        self.lot_id = False


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    @api.multi
    def action_confirm(self):
        super(SaleOrder, self).action_confirm()
        for res in self:
            lot_product = res.order_line.mapped('product_id.tracking')
            if 'lot' not in lot_product:
                for rec in res.order_line:
                    if rec.product_id.tracking == 'serial':
                        if any([line.lot_id for line in self.order_line if len(line.lot_id) != line.product_uom_qty]):
                            raise ValidationError(_('Your Order Quantity Select But Not Available In Lot Number.'))
                        else:
                            for picking in res.picking_ids:
                                for m_line in picking.move_line_ids:
                                    count = 0
                                    for m in m_line.move_id.sale_line_id.lot_id:
                                        m_line.lot_id = m_line.move_id.sale_line_id.lot_id and m_line.move_id.sale_line_id.lot_id.ids[count] or False
                                        m_line.qty_done = 1
                                    count += 1
                                # picking.button_validate()


class SaleOrderLineInventory(models.Model):
    _name = 'sale.order.line.inventory'

    sale_id = fields.Many2one('sale.order', 'Sale Order')
    product_id = fields.Many2one('product.product', 'Product')
    picking_id = fields.Many2one('stock.picking', 'Picking')
    move_id = fields.Many2one('stock.move', 'Move')
    sale_lot_ids = fields.Many2many('stock.production.lot', 'lot_report_lot_rel', 'lot_report_id', 'lot_id', string='Sale Lot')
    move_lot_ids = fields.Many2many('stock.production.lot', string='Move Lot')
