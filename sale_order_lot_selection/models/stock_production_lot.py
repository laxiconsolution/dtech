# -*- coding: utf-8 -*-

from odoo import models, api


class StockProductionLot(models.Model):
    _inherit = 'stock.production.lot'

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        if 'product_id' in self._context:
            args = args or []
            lot_id = self.env['stock.quant'].search(
                                                        [
                                                            ('product_id', '=', self._context.get('product_id')),
                                                            ('location_id.usage', '=', 'internal')
                                                        ]
                                                    ).filtered(lambda x: x.lot_id.product_qty > 0).mapped('lot_id').ids
            args += [('id', 'in', lot_id)]
            recs = self.search(args, limit=limit)
            return recs.name_get()
        return super(StockProductionLot, self).name_search(name, args=args, operator='ilike', limit=100)
