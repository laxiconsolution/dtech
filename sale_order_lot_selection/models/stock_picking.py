# -*- coding: utf-8 -*-

from odoo import api, models


class StockPicking(models.Model):
    _inherit = 'stock.picking'

    @api.multi
    def action_done(self):
        res = super(StockPicking, self).action_done()
        if self.state == 'done' and self.picking_type_code == 'outgoing':
            sol_ids = {}
            for line in self.move_line_ids.filtered(lambda x: x.lot_id):
                if line.move_id.sale_line_id not in sol_ids:
                    sol_ids.update(
                        {line.move_id.sale_line_id: [line.lot_id.id]})
                else:
                    sol_ids[line.move_id.sale_line_id].append(line.lot_id.id)
            for key, vals in sol_ids.items():
                key.lot_id = False
                key.lot_id = [(4, v)for v in vals]
        return res
