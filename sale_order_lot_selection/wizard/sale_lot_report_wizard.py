# -*- coding: utf-8 -*-
from odoo import models, api


class SaleLotReportWiz(models.TransientModel):
    _name = 'sale.lot.report.wiz'

    @api.multi
    def action_lot_report(self):
        self._cr.execute("delete from sale_order_line_inventory")
        picking_data = self.env['stock.picking'].search([('picking_type_code', '=', 'outgoing'), ('state', '!=', 'cancel')])
        data = []
        for picking in picking_data:
            for move in picking.move_ids_without_package:
                if move.product_id.tracking == 'serial':
                    data.append({
                            'sale_id': move.sale_line_id.order_id.id,
                            'picking_id': picking.id,
                            'move_id': move.id,
                            'product_id': move.product_id.id,
                            'sale_lot_ids': [(4, m.id)for m in move.sale_line_id.lot_id if m],
                            'move_lot_ids': [(4, m.lot_id.id)for m in move.move_line_ids if m.lot_id],
                        })
        for d in data:
            self.env['sale.order.line.inventory'].create(d)
        action = self.env.ref('sale_order_lot_selection.action_sale_lot_report').read()[0]
        return action

    @api.multi
    def action_cancel(self):
        return True
