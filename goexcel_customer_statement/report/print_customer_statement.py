from odoo import models, api
from datetime import datetime
import dateutil.relativedelta
from datetime import timedelta, date
import calendar
from odoo.tools.misc import formatLang
import math
import logging
_logger = logging.getLogger(__name__)


class print_customer_statement(models.AbstractModel):
    _name = 'report.goexcel_customer_statement.cust_statement_template'

    @api.multi
    def get_company_info(self, o):
        if self.env.user.company_id:
            company_info = {
                    'image': self.env.user.company_id,
                    'name': self.env.user.company_id.name,
                    'street': self.env.user.company_id.street,
                    'city': str(self.env.user.company_id.city) + ' ' + str(self.env.user.company_id.zip),
                    'state': self.env.user.company_id.state_id and self.env.user.company_id.state_id.name,
                    'country': self.env.user.company_id.country_id and self.env.user.company_id.country_id.name,
            }
        return company_info

    @api.multi
    def set_amount(self, amount):
        amount = formatLang(self.env, amount)
        return amount

    def get_month_name(self, day, mon, year):
        year = str(year)
        day = str(day)
        if mon == 1:
            return day + ' - ' + 'JAN' + ' - ' + year
        elif mon == 2:
            return day + ' - ' + 'FEB' + ' - ' + year
        elif mon == 3:
            return day + ' - ' + 'MAR' + ' - ' + year
        elif mon == 4:
            return day + ' - ' + 'APR' + ' - ' + year
        elif mon == 5:
            return day + ' - ' + 'MAY' + ' - ' + year
        elif mon == 6:
            return day + ' - ' + 'JUN' + ' - ' + year
        elif mon == 7:
            return day + ' - ' + 'JUL' + ' - ' + year
        elif mon == 8:
            return day + ' - ' + 'AUG' + ' - ' + year
        elif mon == 9:
            return day + ' - ' + 'SEP' + ' - ' + year
        elif mon == 10:
            return day + ' - ' + 'OCT' + ' - ' + year
        elif mon == 11:
            return day + ' - ' + 'NOV' + ' - ' + year
        elif mon == 12:
            return day + ' - ' + 'DEC' + ' - ' + year

    # Bug fixed for Aging Month (Molicc 10/2/2021)
    def set_ageing(self, obj):
        # print('set_ageing')
        move_lines = self.get_lines_aging(obj)

        over_date = obj.overdue_date
        # print("over_date=" + str(over_date))
        d5 = con5 = False
        if obj.aging_group == 'by_month':
            d1 = over_date - dateutil.relativedelta.relativedelta(months=1)
            d1 = datetime(d1.year, d1.month, 1) + timedelta(days=calendar.monthrange(d1.year, d1.month)[1] - 1)
            d1 = d1.date()
            d2 = over_date - dateutil.relativedelta.relativedelta(months=2)
            d2 = datetime(d2.year, d2.month, 1) + timedelta(days=calendar.monthrange(d2.year, d2.month)[1] - 1)
            d2 = d2.date()
            d3 = over_date - dateutil.relativedelta.relativedelta(months=3)
            d3 = datetime(d3.year, d3.month, 1) + timedelta(days=calendar.monthrange(d3.year, d3.month)[1] - 1)
            d3 = d3.date()
            d4 = over_date - dateutil.relativedelta.relativedelta(months=4)
            d4 = datetime(d4.year, d4.month, 1) + timedelta(days=calendar.monthrange(d4.year, d4.month)[1] - 1)
            d4 = d4.date()
            d5 = over_date - dateutil.relativedelta.relativedelta(months=5)
            d5 = datetime(d5.year, d5.month, 1) + timedelta(days=calendar.monthrange(d5.year, d5.month)[1] - 1)
            d5 = d5.date()
        else:
            d1 = over_date - timedelta(days=30)
            d2 = over_date - timedelta(days=60)
            d3 = over_date - timedelta(days=90)
            d4 = over_date - timedelta(days=120)

        # con1 = 1st month, con2 = 2nd month, etc.
        con1 = int(str(over_date - d1).split(' ')[0])
        con2 = int(str(over_date - d2).split(' ')[0])
        con3 = int(str(over_date - d3).split(' ')[0])
        con4 = int(str(over_date - d4).split(' ')[0])
        # con5 = int(str(over_date - d5).split(' ')[0])

        f1 = self.get_month_name(over_date.day, over_date.month, over_date.year)
        d1 = self.get_month_name(d1.day, d1.month, d1.year)
        d2 = self.get_month_name(d2.day, d2.month, d2.year)
        d3 = self.get_month_name(d3.day, d3.month, d3.year)
        d4 = self.get_month_name(d4.day, d4.month, d4.year) + ' (UPTO)'
        if d5:
            con5 = int(str(over_date - d5).split(' ')[0])
            d5 = self.get_month_name(d5.day, d5.month, d5.year)

        not_due = 0.0
        f_pe = 0.0  # 0 -30
        s_pe = 0.0  # 31-60
        t_pe = 0.0  # 61-90
        fo_pe = 0.0  # 91-120
        fi_pe = 0.0  # 120 - 150
        l_pe = 0.0  # +150

        for line in move_lines:
            ag_date = False
            if obj.aging_by == 'due_date':
                ag_date = line.get('date_maturity')
            else:
                ag_date = line.get('date')
            if ag_date and obj.overdue_date:
                due_date = ag_date
                # print("due_date=" + str(due_date))
                over_date = obj.overdue_date
                if over_date != due_date:
                    if not ag_date > obj.overdue_date:
                        days = over_date - due_date
                        days = int(str(days).split(' ')[0])
                    else:
                        days = -1
                else:
                    days = 0
                if line.get('total') > 0:
                    if days < 0:
                        not_due += line.get('total')
                    elif days < con1:
                        f_pe += line.get('total')
                    elif days < con2:
                        s_pe += line.get('total')
                    elif days < con3:
                        t_pe += line.get('total')
                    elif days < con4:
                        fo_pe += line.get('total')
                    elif days < con5 and d5:
                        fi_pe += line.get('total')
                    else:
                        l_pe += line.get('total')
                        print("last month due=" + str(l_pe))
        total = 0
        # print('get_balance_bf in set_ageing')
        bf_balance_lines = self.get_balance_bf(obj)
        for balance_line in bf_balance_lines:
            # balance_bf = balance_line.get('total')
            # total += balance_bf
            # # bf_balance_days = (obj.overdue_date - obj.invoice_start_date).days + 1
            # date_maturity = balance_line.get('date_maturity')
            # # date_maturity = balance_line.get('invoice_date')
            # bf_balance_days = (obj.overdue_date - date_maturity).days + 1
            # print('balance_bf: ' + str(balance_bf) + ' , date_maturity:' + str(date_maturity) + ' , balance_day=' + str(bf_balance_days))
            # # print("bf_balance_days=" + str(bf_balance_days) + ', con1=' + str(con1) + ' , con2=' + str(con2)
            # #             + ' , con3=' + str(con3))
            # if bf_balance_days < con1:
            #     f_pe += balance_bf
            #     print("first month due=" + str(f_pe) + ' , con1=' + str(con1))
            # elif bf_balance_days < con2:
            #     s_pe += balance_bf
            #     print("second month due=" + str(s_pe) + ' , con1=' + str(con2))
            # elif bf_balance_days < con3:
            #     t_pe += balance_bf
            #     print("third month due=" + str(t_pe) + ' , con1=' + str(con3))
            # elif bf_balance_days < con4:
            #     fo_pe += balance_bf
            #     print("fourth month due=" + str(fo_pe) + ' , con1=' + str(con4))
            # elif bf_balance_days < con5:
            #     fi_pe += balance_bf
            # else:
            #     l_pe += balance_bf
            #     # print("last=" + str(l_pe))
            total += balance_line.get('total')
            ag_date = False
            if obj.aging_by == 'due_date':
                ag_date = balance_line.get('date_maturity')
            else:
                ag_date = balance_line.get('invoice_date')
            if ag_date and obj.overdue_date:
                due_date = ag_date
                over_date = obj.overdue_date
                if over_date != due_date:
                    if not ag_date > obj.overdue_date:
                        bf_balance_days = obj.overdue_date - due_date
                        bf_balance_days = int(str(bf_balance_days).split(' ')[0])
                    else:
                        bf_balance_days = -1
                else:
                    bf_balance_days = 0
                balance_bf = balance_line.get('total')
                if bf_balance_days <= con1:
                    f_pe += balance_bf
                elif bf_balance_days <= con2:
                    s_pe += balance_bf
                elif bf_balance_days <= con3:
                    t_pe += balance_bf
                elif bf_balance_days <= con4:
                    fo_pe += balance_bf
                elif bf_balance_days <= con5 and d5:
                    fi_pe += balance_bf
                else:
                    l_pe += balance_bf
        if d5:
            return [{
                'not_due': not_due,
                f1: f_pe,
                d1: s_pe,
                d2: t_pe,
                d3: fo_pe,
                d4: fi_pe,
                d5: l_pe,
                total: total,
            }, [f1, d1, d2, d3, d4, d5, total]]
        return [{
            'not_due': not_due,
            f1: f_pe,
            d1: s_pe,
            d2: t_pe,
            d3: fo_pe,
            d4: fi_pe,
            total: total
        }, [f1, d1, d2, d3, d4, total]]

    # TS Bug - fixed Molicc 17/2/2021
    def set_month_ageing(self, obj):
        # print('set_ageing')
        move_lines = self.get_lines_aging(obj)

        over_date = obj.overdue_date
        # print("over_date=" + str(over_date))
        d1 = over_date - dateutil.relativedelta.relativedelta(months=1)
        d1 = datetime(d1.year, d1.month, 1) + timedelta(days=calendar.monthrange(d1.year, d1.month)[1] - 1)
        d1 = d1.date()
        d2 = over_date - dateutil.relativedelta.relativedelta(months=2)
        d2 = datetime(d2.year, d2.month, 1) + timedelta(days=calendar.monthrange(d2.year, d2.month)[1] - 1)
        d2 = d2.date()
        d3 = over_date - dateutil.relativedelta.relativedelta(months=3)
        d3 = datetime(d3.year, d3.month, 1) + timedelta(days=calendar.monthrange(d3.year, d3.month)[1] - 1)
        d3 = d3.date()
        d4 = over_date - dateutil.relativedelta.relativedelta(months=4)
        d4 = datetime(d4.year, d4.month, 1) + timedelta(days=calendar.monthrange(d4.year, d4.month)[1] - 1)
        d4 = d4.date()
        d5 = over_date - dateutil.relativedelta.relativedelta(months=5)
        d5 = datetime(d5.year, d5.month, 1) + timedelta(days=calendar.monthrange(d5.year, d5.month)[1] - 1)
        d5 = d5.date()

        # con1 = 1st month, con2 = 2nd month, etc.
        con1 = int(str(over_date - d1).split(' ')[0])
        con2 = int(str(over_date - d2).split(' ')[0])
        con3 = int(str(over_date - d3).split(' ')[0])
        con4 = int(str(over_date - d4).split(' ')[0])
        con5 = int(str(over_date - d5).split(' ')[0])

        f1 = self.get_month_name(over_date.day, over_date.month, over_date.year)
        d1 = self.get_month_name(d1.day, d1.month, d1.year)
        d2 = self.get_month_name(d2.day, d2.month, d2.year)
        d3 = self.get_month_name(d3.day, d3.month, d3.year)
        d4 = self.get_month_name(d4.day, d4.month, d4.year) + ' (UPTO)'
        d5 = self.get_month_name(d5.day, d5.month, d5.year)

        not_due = 0.0
        f_pe = 0.0  # 0 -30
        s_pe = 0.0  # 31-60
        t_pe = 0.0  # 61-90
        fo_pe = 0.0  # 91-120
        fi_pe = 0.0  # 120 - 150
        l_pe = 0.0  # +150

        for line in move_lines:
            ag_date = False
            # if obj.aging_by == 'due_date':
            #     ag_date = line.get('date_maturity')
            # else:
            ag_date = line.get('date')
            if ag_date and obj.overdue_date:
                due_date = ag_date
                # print("due_date=" + str(due_date))
                over_date = obj.overdue_date
                if over_date != due_date:
                    if not ag_date > obj.overdue_date:
                        days = over_date - due_date
                        days = int(str(days).split(' ')[0])
                    else:
                        days = -1
                else:
                    days = 0
                print("days=" + str(days) + ' , total=' + str(line.get('total')))
                if line.get('total') > 0:
                    if days < 0:
                        not_due += line.get('total')
                        print("not due=" + str(not_due))
                    elif days < con1:
                        f_pe += line.get('total')
                        print("first month due=" + str(f_pe))
                    elif days < con2:
                        s_pe += line.get('total')
                        print("second month due=" + str(s_pe))
                    elif days < con3:
                        t_pe += line.get('total')
                        print("third month due=" + str(t_pe))
                    elif days < con4:
                        fo_pe += line.get('total')
                        print("fourth month due=" + str(fo_pe))
                    elif days < con5:
                        fi_pe += line.get('total')
                    else:
                        l_pe += line.get('total')
                        print("last month due=" + str(l_pe))

        # print('get_balance_bf in set_ageing')
        bf_balance_lines = self.get_balance_bf(obj)
        for balance_line in bf_balance_lines:
            balance_bf = balance_line.get('total')
            # bf_balance_days = (obj.overdue_date - obj.invoice_start_date).days + 1
            date_maturity = balance_line.get('date_maturity')
            # date_maturity = balance_line.get('invoice_date')
            bf_balance_days = (obj.overdue_date - date_maturity).days + 1
            print('balance_bf: ' + str(balance_bf) + ' , date_maturity:' + str(date_maturity) + ' , balance_day=' + str(bf_balance_days))
            # print("bf_balance_days=" + str(bf_balance_days) + ', con1=' + str(con1) + ' , con2=' + str(con2)
            #             + ' , con3=' + str(con3))
            if bf_balance_days < con1:
                f_pe += balance_bf
                print("first month due=" + str(f_pe) + ' , con1=' + str(con1))
            elif bf_balance_days < con2:
                s_pe += balance_bf
                print("second month due=" + str(s_pe) + ' , con1=' + str(con2))
            elif bf_balance_days < con3:
                t_pe += balance_bf
                print("third month due=" + str(t_pe) + ' , con1=' + str(con3))
            elif bf_balance_days < con4:
                fo_pe += balance_bf
                print("fourth month due=" + str(fo_pe) + ' , con1=' + str(con4))
            elif bf_balance_days < con5:
                fi_pe += balance_bf
            else:
                l_pe += balance_bf
                # print("last=" + str(l_pe))

        return [{
            'not_due': not_due,
            f1: f_pe,
            d1: s_pe,
            d2: t_pe,
            d3: fo_pe,
            d4: fi_pe,
            d5: l_pe,
        }, [f1, d1, d2, d3, d4, d5]]

    # def set_ageing_cumulative(self, obj):
    #     move_lines=self.get_lines(obj)
    #     over_date=obj.overdue_date
    #     #_logger.warning("over_date=" + str(over_date))
    #     d1=over_date - dateutil.relativedelta.relativedelta(months=1)
    #     d1=datetime(d1.year,d1.month,1) + timedelta(days=calendar.monthrange(d1.year,d1.month)[1] - 1)
    #     d1 = d1.date()
    #     d2=over_date - dateutil.relativedelta.relativedelta(months=2)
    #     d2=datetime(d2.year,d2.month,1) + timedelta(days=calendar.monthrange(d2.year,d2.month)[1] - 1)
    #     d2 = d2.date()
    #     d3=over_date - dateutil.relativedelta.relativedelta(months=3)
    #     d3=datetime(d3.year,d3.month,1) + timedelta(days=calendar.monthrange(d3.year,d3.month)[1] - 1)
    #     d3 = d3.date()
    #     d4=over_date - dateutil.relativedelta.relativedelta(months=4)
    #     d4=datetime(d4.year,d4.month,1) + timedelta(days=calendar.monthrange(d4.year,d4.month)[1] - 1)
    #     d4 = d4.date()
    #     d5=over_date - dateutil.relativedelta.relativedelta(months=5)
    #     d5=datetime(d5.year,d5.month,1) + timedelta(days=calendar.monthrange(d5.year,d5.month)[1] - 1)
    #     d5 = d5.date()
    
    #     #con1 = 1st month, con2 = 2nd month, etc.
    #     con1 = int(str(over_date - d1).split(' ')[0])
    #     con2 = int(str(over_date - d2).split(' ')[0])
    #     con3 = int(str(over_date - d3).split(' ')[0])
    #     con4 = int(str(over_date - d4).split(' ')[0])
    #     con5 = int(str(over_date - d5).split(' ')[0])
    
    #     f1 = self.get_month_name(over_date.day,over_date.month,over_date.year)
    #     d1 = self.get_month_name(d1.day,d1.month,d1.year)
    #     d2 = self.get_month_name(d2.day,d2.month,d2.year)
    #     d3 = self.get_month_name(d3.day,d3.month,d3.year)
    #     d4 = self.get_month_name(d4.day,d4.month,d4.year) + ' (UPTO)'
    #     d5 = self.get_month_name(d5.day,d5.month,d5.year)
    
    #     not_due = 0.0
    #     f_pe = 0.0 # 0 -30
    #     s_pe = 0.0 # 31-60
    #     t_pe = 0.0 # 61-90
    #     fo_pe = 0.0 # 91-120
    #     l_pe = 0.0 # +120
    
    #     for line in move_lines:
    #         ag_date=False
    #         if obj.aging_by == 'due_date':
    #             ag_date = line.get('date_maturity')
    #         else:
    #             ag_date = line.get('date')
    #         if ag_date and obj.overdue_date:
    #             due_date=ag_date
    #             #_logger.warning("due_date=" + str(due_date))
    #             over_date=obj.overdue_date
    #             if over_date != due_date:
    #                 if not ag_date > obj.overdue_date:
    #                     days=over_date - due_date
    #                     days=int(str(days).split(' ')[0])
    #                 else:
    #                     days= -1
    #             else:
    #                 days = 0
    #             #_logger.warning("days=" + str(days) + ' , total=' + str(line.get('total')))
    #             if days < 0:
    #                 not_due += line.get('total')
    #                 #_logger.warning("not due=" + str(not_due))
    #             elif days < con1:
    #                 f_pe += line.get('total')
    #                 #_logger.warning("first month due=" + str(f_pe))
    #             elif days < con2:
    #                 s_pe += line.get('total')
    #                 #_logger.warning("second month due=" + str(s_pe))
    #             elif days < con3:
    #                 t_pe += line.get('total')
    #                 #_logger.warning("third month due=" + str(t_pe))
    #             elif days < con4:
    #                 fo_pe += line.get('total')
    #                 #_logger.warning("fourth month due=" + str(fo_pe))
    #             else:
    #                 l_pe += line.get('total')
    #                 #_logger.warning("last month due=" + str(l_pe))
    
    #     bf_balance_lines = self.get_balance_bf(obj)
    #     for balance_line in bf_balance_lines:
    #         balance_bf = balance_line.get('total')
    #         bf_balance_days = (obj.overdue_date - obj.invoice_start_date).days + 1
    #         # _logger.warning("bf_balance_days=" + str(bf_balance_days) + ', con1=' + str(con1) + ' , con2=' + str(con2)
    #         #                + ' , con3=' + str(con3))
    #         if bf_balance_days < con1:
    #             f_pe = balance_bf
    #         #    _logger.warning("first month due=" + str(f_pe))
    #         elif bf_balance_days < con2:
    #             s_pe = balance_bf
    #         #    _logger.warning("second month due=" + str(s_pe))
    #         elif bf_balance_days < con3:
    #             t_pe = balance_bf
    #         #    _logger.warning("third month due=" + str(t_pe))
    #         elif bf_balance_days < con4:
    #             fo_pe = balance_bf
    #         #   _logger.warning("fourth month due=" + str(fo_pe))
    #         else:
    #             l_pe = balance_bf
    #         #   _logger.warning("last=" + str(l_pe))
    
    #     fo_pe += l_pe
    #     t_pe += fo_pe
    #     s_pe += t_pe
    #     f_pe += s_pe
    
    
    #     return [{
    #             'not_due':not_due,
    #             f1: f_pe,
    #             d1: s_pe,
    #             d2: t_pe,
    #             d3: fo_pe,
    #             d4: l_pe,
    #         },[f1,d1,d2,d3,d4]]

    # get only the account move lines for the selected date
    def _lines_get(self, partner):
        moveline_obj = self.env['account.move.line']
        move_lines = moveline_obj.search([('partner_id', '=', partner.id),
                                          # ('date', '<=', partner.overdue_date),
                                          # ('date', '>=', partner.invoice_start_date),
                                          ('account_id.user_type_id.type', 'in', ['receivable', 'payable']),
                                          ('move_id.state', '<>', 'draft')])
        # movelines = []
        # for move in move_lines:
        #     m_date = move.date
        #     i_date = move.invoice_id.date_invoice
        #     if partner.aging_by == 'due_date':
        #         m_date = move.date_maturity
        #         i_date = move.invoice_id.date_due
        #     if m_date <= partner.overdue_date and m_date >= partner.invoice_start_date or move.invoice_id and i_date <= partner.overdue_date and i_date >= partner.invoice_start_date:
        #         movelines.append(move.id)
        #     if move.payment_id and move.payment_id.invoice_ids.filtered(lambda y: y.date_invoice <= partner.overdue_date and y.date_invoice >= partner.invoice_start_date) and move.id not in movelines:
        #         movelines.append(move.id)
        movelines = move_lines.filtered(lambda x: x.date <= partner.overdue_date and x.date >= partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.invoice_id and x.invoice_id.date_invoice <= partner.overdue_date and x.invoice_id.date_invoice >= partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.payment_id and x.payment_id.invoice_ids.filtered(lambda y: y.date_invoice <= partner.overdue_date and y.date_invoice >= partner.invoice_start_date))
        # return moveline_obj.browse(movelines)
        return movelines

        # get only the account move lines for the selected date
    def _lines_get_receivable(self, partner):
        moveline_obj = self.env['account.move.line']
        move_lines = moveline_obj.search([('partner_id', '=', partner.id),
                                          # ('date', '<=', partner.overdue_date),
                                          # ('date', '>=', partner.invoice_start_date),
                                          ('account_id.user_type_id.type', '=', 'receivable'),
                                          ('move_id.state', '<>', 'draft')])
        # movelines = []
        # for move in move_lines:
        #     m_date = move.date
        #     i_date = move.invoice_id.date_invoice
        #     if partner.aging_by == 'due_date':
        #         m_date = move.date_maturity
        #         i_date = move.invoice_id.date_due
        #     if m_date <= partner.overdue_date and m_date >= partner.invoice_start_date or move.invoice_id and i_date <= partner.overdue_date and i_date >= partner.invoice_start_date:
        #         movelines.append(move.id)
        #     if move.payment_id and move.payment_id.invoice_ids.filtered(lambda y: y.date_invoice <= partner.overdue_date and y.date_invoice >= partner.invoice_start_date) and move.id not in movelines:
        #         movelines.append(move.id)
        movelines = move_lines.filtered(lambda x: x.date <= partner.overdue_date and x.date >= partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.invoice_id and x.invoice_id.date_invoice <= partner.overdue_date and x.invoice_id.date_invoice >= partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.payment_id and x.payment_id.invoice_ids.filtered(lambda y: y.date_invoice <= partner.overdue_date and y.date_invoice >= partner.invoice_start_date))
        # return moveline_obj.browse(movelines)
        return movelines

    def _lines_get_payable(self, partner):
        moveline_obj = self.env['account.move.line']
        move_lines = moveline_obj.search([('partner_id', '=', partner.id),
                                          # ('date', '<=', partner.overdue_date),
                                          # ('date', '>=', partner.invoice_start_date),
                                          ('account_id.user_type_id.type', '=', 'payable'),
                                          ('move_id.state', '<>', 'draft')])
        # movelines = []
        # for move in move_lines:
        #     m_date = move.date
        #     i_date = move.invoice_id.date_invoice
        #     if partner.aging_by == 'due_date':
        #         m_date = move.date_maturity
        #         i_date = move.invoice_id.date_due
        #     if m_date <= partner.overdue_date and m_date >= partner.invoice_start_date or move.invoice_id and i_date <= partner.overdue_date and i_date >= partner.invoice_start_date:
        #         movelines.append(move.id)
        #     if move.payment_id and move.payment_id.invoice_ids.filtered(lambda y: y.date_invoice <= partner.overdue_date and y.date_invoice >= partner.invoice_start_date) and move.id not in movelines:
        #         movelines.append(move.id)
        movelines = move_lines.filtered(lambda x: x.date <= partner.overdue_date and x.date >= partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.invoice_id and x.invoice_id.date_invoice <= partner.overdue_date and x.invoice_id.date_invoice >= partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.payment_id and x.payment_id.invoice_ids.filtered(lambda y: y.date_invoice <= partner.overdue_date and y.date_invoice >= partner.invoice_start_date))
        # return moveline_obj.browse(movelines)
        return movelines

    # get the bring forward balance
    def _lines_get_all(self, partner):
        moveline_obj = self.env['account.move.line']
        move_lines = moveline_obj.search([('partner_id', '=', partner.id),
                                          # ('date', '<', partner.invoice_start_date),
                                          ('account_id.user_type_id.type', 'in', ['receivable', 'payable']),
                                          ('move_id.state', '<>', 'draft')])
        # movelines = []
        # for move in move_lines:
        #     m_date = move.date
        #     i_date = move.invoice_id.date_invoice
        #     if partner.aging_by == 'due_date':
        #         m_date = move.date_maturity
        #         i_date = move.invoice_id.date_due
        #     if m_date < partner.invoice_start_date or move.invoice_id and i_date < partner.invoice_start_date:
        #         movelines.append(move.id)
        #     if move.payment_id and move.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.invoice_start_date) and move.id not in movelines:
        #         movelines.append(move.id)
        movelines = move_lines.filtered(lambda x: x.date < partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.invoice_id and x.invoice_id.date_invoice < partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.payment_id and x.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.invoice_start_date))
        # return moveline_obj.browse(movelines)
        return movelines

    # get the bring forward balance
    def _lines_get_all_receivable(self, partner):
        moveline_obj = self.env['account.move.line']
        move_lines = moveline_obj.search([('partner_id', '=', partner.id),
                                          # # ('date', '>', partner.invoice_start_date),
                                          # ('date', '<', partner.invoice_start_date),
                                          ('account_id.user_type_id.type', '=', 'receivable'),
                                          ('move_id.state', '<>', 'draft')])
        # movelines = []
        # for move in move_lines:
        #     m_date = move.date
        #     i_date = move.invoice_id.date_invoice
        #     if partner.aging_by == 'due_date':
        #         m_date = move.date_maturity
        #         i_date = move.invoice_id.date_due
        #     if m_date < partner.invoice_start_date or move.invoice_id and i_date < partner.invoice_start_date:
        #         movelines.append(move.id)
        #     if move.payment_id and move.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.invoice_start_date) and move.id not in movelines:
        #         movelines.append(move.id)
        movelines = move_lines.filtered(lambda x: x.date < partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.invoice_id and x.invoice_id.date_invoice < partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.payment_id and x.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.invoice_start_date))
        # return moveline_obj.browse(movelines)
        return movelines

    # get the bring forward balance
    def _lines_get_all_payable(self, partner):
        moveline_obj = self.env['account.move.line']
        move_lines = moveline_obj.search([('partner_id', '=', partner.id),
                                          # ('date', '<', partner.invoice_start_date),
                                          ('account_id.user_type_id.type', '=', 'payable'),
                                          ('move_id.state', '<>', 'draft')])
        # movelines = []
        # for move in move_lines:
        #     m_date = move.date
        #     i_date = move.invoice_id.date_invoice
        #     if partner.aging_by == 'due_date':
        #         m_date = move.date_maturity
        #         i_date = move.invoice_id.date_due
        #     if m_date < partner.invoice_start_date or move.invoice_id and i_date < partner.invoice_start_date:
        #         movelines.append(move.id)
        #     if move.payment_id and move.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.invoice_start_date) and move.id not in movelines:
        #         movelines.append(move.id)
        movelines = move_lines.filtered(lambda x: x.date < partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.invoice_id and x.invoice_id.date_invoice < partner.invoice_start_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.payment_id and x.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.invoice_start_date))
        # return moveline_obj.browse(movelines)
        return movelines

    def _lines_get_receivable_end_date(self, partner):
        moveline_obj = self.env['account.move.line']
        move_lines = moveline_obj.search([('partner_id', '=', partner.id),
                                          # ('date', '>', partner.invoice_start_date),
                                          ('date', '<', partner.overdue_date),
                                          ('account_id.user_type_id.type', '=', 'receivable'),
                                          ('move_id.state', '<>', 'draft')])
        # movelines = []
        # for move in move_lines:
        #     m_date = move.date
        #     i_date = move.invoice_id.date_invoice
        #     if partner.aging_by == 'due_date':
        #         m_date = move.date_maturity
        #         i_date = move.invoice_id.date_due
        #     if m_date < partner.overdue_date or move.invoice_id and i_date < partner.overdue_date:
        #         movelines.append(move.id)
        #     if move.payment_id and move.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.overdue_date) and move.id not in movelines:
        #         movelines.append(move.id)
        movelines = move_lines.filtered(lambda x: x.date < partner.overdue_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.invoice_id and x.invoice_id.date_invoice < partner.overdue_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.payment_id and x.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.overdue_date))
        # return moveline_obj.browse(movelines)
        return movelines

    def _lines_get_payable_end_date(self, partner):
        moveline_obj = self.env['account.move.line']
        move_lines = moveline_obj.search([('partner_id', '=', partner.id),
                                          # ('date', '>', partner.invoice_start_date),
                                          ('date', '<', partner.overdue_date),
                                          ('account_id.user_type_id.type', '=', 'payable'),
                                          ('move_id.state', '<>', 'draft')])
        # movelines = []
        # for move in move_lines:
        #     m_date = move.date
        #     i_date = move.invoice_id.date_invoice
        #     if partner.aging_by == 'due_date':
        #         m_date = move.date_maturity
        #         i_date = move.invoice_id.date_due
        #     if m_date < partner.overdue_date or move.invoice_id and i_date < partner.overdue_date:
        #         movelines.append(move.id)
        #     if move.payment_id and move.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.overdue_date) and move.id not in movelines:
        #         movelines.append(move.id)
        movelines = move_lines.filtered(lambda x: x.date < partner.overdue_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.invoice_id and x.invoice_id.date_invoice < partner.overdue_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.payment_id and x.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.overdue_date))
        # return moveline_obj.browse(movelines)
        return movelines

    def _lines_get_all_end_date(self, partner):
        moveline_obj = self.env['account.move.line']
        move_lines = moveline_obj.search([('partner_id', '=', partner.id),
                                          # ('date', '>', partner.invoice_start_date),
                                          ('date', '<', partner.overdue_date),
                                          ('account_id.user_type_id.type', 'in', ['receivable', 'payable']),
                                          ('move_id.state', '<>', 'draft')])
        # movelines = []
        # for move in move_lines:
        #     m_date = move.date
        #     i_date = move.invoice_id.date_invoice
        #     if partner.aging_by == 'due_date':
        #         m_date = move.date_maturity
        #         i_date = move.invoice_id.date_due
        #     if m_date < partner.overdue_date or move.invoice_id and i_date < partner.overdue_date:
        #         movelines.append(move.id)
        #     if move.payment_id and move.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.overdue_date) and move.id not in movelines:
        #         movelines.append(move.id)
        movelines = move_lines.filtered(lambda x: x.date < partner.overdue_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.invoice_id and x.invoice_id.date_invoice < partner.overdue_date)
        movelines += move_lines.filtered(lambda x: x.id not in movelines.ids and x.payment_id and x.payment_id.invoice_ids.filtered(lambda y: y.date_invoice < partner.overdue_date))
        # return moveline_obj.browse(movelines)
        return movelines

    # get the bring forward balance
    def get_balance_bf(self, partner):
        # _logger.warning('get_lines_all')
        if partner.account_type == 'ar':
            move_lines = self._lines_get_receivable_end_date(partner)
        if partner.account_type == 'ap':
            move_lines = self._lines_get_payable_end_date(partner)
        elif partner.account_type == 'both':
            move_lines = self._lines_get_all_end_date(partner)
        # for p in partner:
        #     if p.invoice_start_date:
        #         self.invoice_start_date = partner.invoice_start_date
        res = []
        total_inv_amt = 0.0
        total_paid_amt = 0.0
        total = 0.0
        if move_lines:
            reversed_move_lines = move_lines.sorted(key=lambda x: x.date)
            for line in reversed_move_lines:
                if not line.payment_id and line.invoice_id.state != 'paid':
                    invoice_paid = False
                    inv_amt = 0.0
                    paid_amt = 0.0
                    # _logger.warning('invoice #=' + line.move_id.name)
                    # populate invoice amount (debit)/credit
                    # if line.account_id.user_type_id.type == 'receivable':
                    if line.debit:
                        inv_amt = line.debit
                    if line.credit:
                        paid_amt = line.credit
                    # print('invoice #=' + line.move_id.name + ' , inv amt= ' + str(inv_amt) + ", paid_amt=" + str(paid_amt))
                    # total_inv_amt += round(inv_amt,2)
                    # if inv_amt < 0:   #for credit note
                    #     paid_amt = math.fabs(paid_amt)
                    #     paid_amt = round(paid_amt, 2)
                    #     inv_amt = 0
                    # total_paid_amt += round(paid_amt, 2)

                    invoices = self.env['account.invoice'].search([
                        ('move_id', '=', line.move_id.id), ])
                    # print('invoice len=' + str(len(invoices)))

                    # TS fixed bug - b/f shld not include paid invoice
                    for invoice in invoices:
                        if invoice.residual == 0 and invoice.type not in ['out_refund', 'in_invoice']:
                            invoice_paid = True
                        # print('invoice_paid - ' + str(invoice.name) + ' , ' + str(invoice_paid) + ' , inv_residual:' + str(invoice.residual))
                    if inv_amt < 0:  # for credit note
                        invoice_paid = False
                    # _logger.warning('total_inv_amt=' + str(total_inv_amt) + ' , total_paid_amt=' + str(total_paid_amt))
                    # populate balance
                    # if invoice.type in ['out_refund', 'in_invoice']:
                    #     total = -(paid_amt)
                    # else:
                    #     total = float(inv_amt)
                    # if not invoice_paid and not line.payment_id and line.invoice_id.state != 'paid' and partner.soa_type != 'all':
                    #     # only add the invoice amount, paid amount is not considered
                    #     # balance_total += float(balance_inv_amt - balance_paid_amt)
                    #     # total += float(inv_amt)
                    #     # print('total=' + str(total), ' , date_maturity:' + str(line.date_maturity))
                    #     if total != 0:
                    #         # self.balance_bf = total
                    #         # _logger.warning('self.balance_bf =' + str(self.balance_bf))
                    #         res.append({
                    #             'ref': 'Balance b/f',
                    #             'invoice_date': line.date,
                    #             'date_maturity': line.date_maturity,
                    #             'debit': float(total_inv_amt),
                    #             'credit': float(total_paid_amt),
                    #             'total': float(total),
                    #         })
                    # elif partner.soa_type == 'all':
                    #     if total != 0:
                    #         res.append({
                    #                 'ref': 'Balance b/f',
                    #                 'invoice_date': line.date,
                    #                 'date_maturity': line.date_maturity,
                    #                 'debit': float(total_inv_amt),
                    #                 'credit': float(total_paid_amt),
                    #                 'total': float(total),
                    #             })
                    if not invoice_paid:
                        # only add the invoice amount, paid amount is not considered
                        # balance_total += float(balance_inv_amt - balance_paid_amt)
                        if invoice.type in ['out_refund', 'in_invoice']:
                            total = -(paid_amt)
                        else:
                            total = float(inv_amt)
                        # total += float(inv_amt)
                        # print('total=' + str(total), ' , date_maturity:' + str(line.date_maturity))
                        if total != 0:
                            # self.balance_bf = total
                            # _logger.warning('self.balance_bf =' + str(self.balance_bf))
                            res.append({
                                'ref': 'Balance b/f',
                                'invoice_date': line.date,
                                'date_maturity': line.date_maturity,
                                'debit': float(total_inv_amt),
                                'credit': float(total_paid_amt),
                                'total': float(total),
                            })
        return res

    # bug fixed
    # for aging data only, because we should not include the paid invoice in the aging
    def get_lines_aging(self, partner):
        # get the balance b/f
        # print('get line aging')
        # if partner.account_type == 'ar':
        #     balance_move_lines = self._lines_get_all_receivable(partner)
        # if partner.account_type == 'ap':
        #     balance_move_lines = self._lines_get_all_payable(partner)
        # elif partner.account_type == 'both':
        #     balance_move_lines = self._lines_get_all(partner)

        if partner.account_type == 'ar':
            balance_move_lines = self._lines_get_receivable_end_date(partner)
        if partner.account_type == 'ap':
            balance_move_lines = self._lines_get_payable_end_date(partner)
        elif partner.account_type == 'both':
            balance_move_lines = self._lines_get_all_end_date(partner)
        # for p in partner:
        #     if p.invoice_start_date:
        #         self.invoice_start_date = partner.invoice_start_date
        res = []
        total_inv_amt = 0.0
        total_paid_amt = 0.0
        balance_total = 0.0
        if balance_move_lines:
            reversed_balance_move_lines = balance_move_lines.sorted(key=lambda x: x.date)
            for line in reversed_balance_move_lines:
                invoice_paid = False
                balance_inv_amt = 0.0
                balance_paid_amt = 0.0
                # print('invoice #=' + line.move_id.name)
                # populate invoice amount (debit)/credit
                # if line.account_id.user_type_id.type == 'receivable':
                if line.debit:
                    balance_inv_amt = line.debit
                if line.credit:
                    balance_paid_amt = line.credit

                total_inv_amt += round(balance_inv_amt, 2)
                if balance_inv_amt < 0:  # for credit note
                    balance_paid_amt = math.fabs(balance_paid_amt)
                    balance_paid_amt = round(balance_paid_amt, 2)
                    balance_inv_amt = 0
                total_paid_amt += round(balance_paid_amt, 2)
                invoices = self.env['account.invoice'].search([
                    ('move_id', '=', line.move_id.id), ])
                # print('invoice len=' + str(len(invoices)))
                # TS fixed bug - b/f shld not include paid invoice
                for invoice in invoices:
                    if invoice.residual == 0:
                        invoice_paid = True
                # print('invoice_paid - ' + str(invoice_paid))
                # _logger.warning('total_inv_amt=' + str(total_inv_amt) + ' , total_paid_amt=' + str(total_paid_amt))
                # populate balance
                if not invoice_paid:
                    # only add the invoice amount, paid amount is not considered
                    # balance_total += float(balance_inv_amt - balance_paid_amt)
                    balance_total += float(balance_inv_amt)
                    # print('total=' + str(balance_total))
                # balance_inv_amt = debit
                # balance_paid_amt = credit
                # balance_total = balance
            # bug - doesnt show the b/f if negative balance

            if balance_total > 0 or balance_total < 0:
                # print('balance_total=' + balance_total)
                res.append({
                    'date': False,
                    'desc': '',
                    'inv_ref': '',
                    'inv_original': '',
                    'nett_weight': '',
                    'unit_price': '',
                    'payment_ref': '',
                    'invoice_prod_cat': '',
                    'ref': 'Balance b/f',
                    'date_maturity': False,
                    'over_due': False,
                    'debit': 0.0,
                    'credit': 0.0,
                    'total': float(balance_total),
                })

        # get the selected date
        if partner.account_type == 'ar':
            move_lines = self._lines_get_receivable(partner)
        if partner.account_type == 'ap':
            move_lines = self._lines_get_payable(partner)
        elif partner.account_type == 'both':
            move_lines = self._lines_get(partner)
        # move_lines=self._lines_get(partner)
        # res=[]
        if move_lines:
            reversed_move_lines = move_lines.sorted(key=lambda x: x.date)
            # print('len reversed_move_lines=' + str(len(reversed_move_lines)))
            for line in reversed_move_lines:
                #     for line in move_lines:
                inv_amt = 0.0
                paid_amt = 0.0
                over_due = False
                # print('get_lines_aging invoice #=' + line.move_id.name)
                # populate invoice amount (debit)
                # if line.account_id.user_type_id.type == 'receivable':
                if line.debit:
                    inv_amt = line.debit
                if line.credit:
                    paid_amt = line.credit
                    # else:
                    #     inv_amt = line.credit * -1
                    if inv_amt > 0:
                        # _logger.warning('today=' + str(date.today()) + ' , date overdue=' + str(line.date_maturity))
                        if date.today() > line.date_maturity:
                            # _logger.warning('today > overdue')
                            over_due = True
                total = 0.0

                inv_amt = round(inv_amt, 2)
                if inv_amt < 0:  # for credit note
                    paid_amt = math.fabs(paid_amt)
                    paid_amt = round(paid_amt, 2)
                    inv_amt = 0
                # paid_amt = round(paid_amt,2)

                # print('inv_amt=' + str(inv_amt) + ' , paid_amt=' + str(paid_amt))
                # _logger.warning('inv_amt=' + str(inv_amt) + ' , paid_amt=' + str(paid_amt))
                # populate balance
                total = float(inv_amt - paid_amt)

                # get the invoice info
                # _logger.warning("line.move_id.id=" + str(line.move_id.id))
                invoices = self.env['account.invoice'].search([
                    ('move_id', '=', line.move_id.id), ])
                invoice_ref = ''
                invoice_prod_cat = ''
                invoice_original = ''
                nett_weight = 0
                unit_price = 0
                payment_ref = ''
                invoice_paid = False
                for invoice in invoices:
                    # print('invoice name:' + str(invoice.name) + ' , invoice no=' + str(invoice.number) + ' - amount=' + str(inv_amt))
                    # print('paid amount:' + str(paid_amt))
                    # invoice_ref = invoice.reference
                    invoice_ref = invoice.number
                    # we should not include the paid invoice in the aging
                    if invoice.residual == 0:
                        invoice_paid = True
                    invoice_original = invoice.origin
                    if invoice.type == 'out_invoice':
                        payment_ref = invoice.origin

                    else:
                        payment_ref = invoice.reference
                    # lines = invoice.invoice_line_ids.sorted(key=lambda r: r.sequence)
                    inv_lines = invoice.invoice_line_ids.filtered((lambda i: i.sequence == 1))
                    for inv_line in inv_lines:
                        nett_weight = inv_line.quantity
                        unit_price = inv_line.price_unit

                # get the payment info
                if paid_amt > 0:
                    payments = self.env['account.payment'].search([('id', '=', line.payment_id.id), ])
                    if payments:
                        # print('payments len=' + str(len(payments)))
                        for payment in payments:
                            invoice_ref = payment.name
                            # print('payment no=' + str(payment.name))
                            # if payment.communication:
                            if payment.reference:
                                payment_ref = payment.reference
                            # payment_ref = payment.communication
                    # print('invoice_paid - ' + str(invoice_paid))
                    if not invoice_paid:
                        res.append({
                            'date': line.date,
                            'desc': line.ref or '/',
                            'inv_ref': invoice_ref or '',
                            'inv_original': invoice_original or '',
                            'nett_weight': nett_weight or '',
                            'unit_price': unit_price or '',
                            'payment_ref': payment_ref or '',
                            'invoice_prod_cat': invoice_prod_cat or '',
                            'ref': line.move_id.name or '',
                            'date_maturity': line.date_maturity,
                            'over_due': over_due,
                            'debit': float(inv_amt),
                            'credit': float(paid_amt),
                            'total': float(total),
                        })

        return res

    # get only the statements for the selected date
    def get_lines(self, partner):
        # get the balance b/f
        # print('get_lines')
        if partner.account_type == 'ar':
            balance_move_lines = self._lines_get_all_receivable(partner)
        if partner.account_type == 'ap':
            balance_move_lines = self._lines_get_all_payable(partner)
        elif partner.account_type == 'both':
            balance_move_lines = self._lines_get_all(partner)
        # for p in partner:
        #     if p.invoice_start_date:
        #         self.invoice_start_date = partner.invoice_start_date
        res = []
        total_inv_amt = 0.0
        total_paid_amt = 0.0
        balance_total = 0.0
        if balance_move_lines:
            reversed_balance_move_lines = balance_move_lines.sorted(key=lambda x: x.date)
            for line in reversed_balance_move_lines:
                balance_inv_amt = 0.0
                balance_paid_amt = 0.0
                # print('invoice #=' + line.move_id.name)
                # populate invoice amount (debit)/credit
                # if line.account_id.user_type_id.type == 'receivable':
                if line.debit:
                    balance_inv_amt = line.debit
                if line.credit:
                    balance_paid_amt = line.credit
                # print('balance_inv_amt=' + str(balance_inv_amt))
                # print('balance_paid_amt=' + str(balance_paid_amt))
                total_inv_amt += round(balance_inv_amt, 2)
                if balance_inv_amt < 0:  # for credit note
                    balance_paid_amt = math.fabs(balance_paid_amt)
                    balance_paid_amt = round(balance_paid_amt, 2)
                    balance_inv_amt = 0
                total_paid_amt += round(balance_paid_amt, 2)
                # _logger.warning('inv_amt=' + str(inv_amt) + ' , paid_amt=' + str(paid_amt))
                # print('total_inv_amt=' + str(total_inv_amt) + ' , total_paid_amt=' + str(total_paid_amt))
                # populate balance
                balance_total += float(balance_inv_amt - balance_paid_amt)
                if line.payment_id or line.invoice_id.state == 'paid' and partner.soa_type != 'all':
                    balance_total -= float(balance_inv_amt - balance_paid_amt)

                # print('balance_total=' + str(balance_total))
                # balance_inv_amt = debit
                # balance_paid_amt = credit
                # balance_total = balance
            # bug - doesnt show the b/f if negative balance
            if balance_total > 0 or balance_total < 0:
                # self.balance_bf = total
                # _logger.warning('self.balance_bf =' + str(self.balance_bf))
                res.append({
                    'date': False,
                    'desc': '',
                    'inv_ref': '',
                    'inv_original': '',
                    'nett_weight': '',
                    'unit_price': '',
                    'payment_ref': '',
                    'invoice_prod_cat': '',
                    'ref': 'Balance b/f',
                    'date_maturity': False,
                    'over_due': False,
                    'debit': 0.0,
                    'credit': 0.0,
                    'total': float(balance_total),
                })

        # get the selected date
        if partner.account_type == 'ar':
            move_lines = self._lines_get_receivable(partner)
        if partner.account_type == 'ap':
            move_lines = self._lines_get_payable(partner)
        elif partner.account_type == 'both':
            move_lines = self._lines_get(partner)
        # move_lines=self._lines_get(partner)
        #  res=[]
        if move_lines:
            reversed_move_lines = move_lines.sorted(key=lambda x: x.date)
            # print('len reversed_move_lines=' + str(len(reversed_move_lines)))
            for line in reversed_move_lines:
                # for line in move_lines:
                inv_amt = 0.0
                paid_amt = 0.0
                over_due = False
                # print('get_lines invoice #=' + line.move_id.name)
                #  populate invoice amount (debit)
                # if line.account_id.user_type_id.type == 'receivable':
                if line.debit:
                    inv_amt = line.debit
                if line.credit:
                    paid_amt = line.credit
                    # else:
                    #     inv_amt = line.credit * -1
                    if inv_amt > 0:
                        # _logger.warning('today=' + str(date.today()) + ' , date overdue=' + str(line.date_maturity))
                        if date.today() > line.date_maturity:
                            # _logger.warning('today > overdue')
                            over_due = True
                total = 0.0
                inv_amt = round(inv_amt, 2)
                if inv_amt < 0:   # for credit note
                    paid_amt = math.fabs(paid_amt)
                    paid_amt = round(paid_amt, 2)
                    inv_amt = 0
                # paid_amt = round(paid_amt,2)

                # print('inv_amt=' + str(inv_amt) + ' , paid_amt=' + str(paid_amt))
                # _logger.warning('inv_amt=' + str(inv_amt) + ' , paid_amt=' + str(paid_amt))
                # populate balance
                total = float(inv_amt - paid_amt)

                # get the invoice info
                # _logger.warning("line.move_id.id=" + str(line.move_id.id))
                invoices = self.env['account.invoice'].search([('move_id', '=', line.move_id.id)])
                invoice_ref = ''
                invoice_prod_cat = ''
                invoice_original = ''
                nett_weight = 0
                unit_price = 0
                payment_ref = ''
                invoice_paid = False
                payment_id = False
                for invoice in invoices:
                    # print('invoice name:' + str(invoice.name) + ' , invoice no=' + str(invoice.number) + ' - amount=' + str(inv_amt))
                    # print('paid amount:' + str(paid_amt))
                    # invoice_ref = invoice.reference
                    invoice_ref = invoice.number
                    invoice_original = invoice.origin
                    if invoice.type == 'out_invoice':
                        payment_ref = invoice.origin

                    else:
                        payment_ref = invoice.reference
                    # lines = invoice.invoice_line_ids.sorted(key=lambda r: r.sequence)
                    inv_lines = invoice.invoice_line_ids.filtered((lambda i: i.sequence == 1))
                    for inv_line in inv_lines:
                        nett_weight = inv_line.quantity
                        unit_price = inv_line.price_unit
                # get the payment info
                if paid_amt > 0:
                    payments = self.env['account.payment'].search([('id', '=', line.payment_id.id)])
                    # _logger.warning('payments len=' + str(len(payments)))
                    payment_id = True if payments else False
                    for payment in payments:
                        invoice_ref = payment.name
                        # _logger.warning('payment no=' + str(payment.name))
                        # if payment.communication:
                        if payment.reference:
                            payment_ref = payment.reference
                            # payment_ref = payment.communication
                res_data = {
                            'date': line.date,
                            'desc': line.ref or '/',
                            'inv_ref': invoice_ref or '',
                            'inv_original': invoice_original or '',
                            'nett_weight': nett_weight or '',
                            'unit_price': unit_price or '',
                            'payment_ref': payment_ref or '',
                            'invoice_prod_cat': invoice_prod_cat or '',
                            'ref': line.move_id.name or '',
                            'date_maturity': line.date_maturity,
                            'over_due': over_due,
                            'debit': float(inv_amt),
                            'credit': float(paid_amt),
                            'total': float(total),
                            'payment_id': payment_id
                        }
                if not line.payment_id and line.invoice_id.state != 'paid' and partner.soa_type != 'all':
                    res.append(res_data)
                elif partner.soa_type == 'all':
                    res.append(res_data)
        return res

    @api.multi
    def _get_report_values(self, docids, data=None):
        # account_type = data['account']['account_type']
        partner_id = self._context.get('default_res_id')
        if partner_id:
            partner_ids = self.env['res.partner'].browse(self._context.get('default_res_id'))
            return {
                'doc_ids': partner_id,
                'doc_model': 'res.partner',
                'docs': partner_ids,
                'get_lines': self.get_lines,
                'set_ageing': self.set_ageing,
                'set_amount': self.set_amount,
                'get_company_info': self.get_company_info,
            }

        else:
            docs = self.env['res.partner'].browse(data['form'])
            return {
                'doc_ids': data['ids'],
                'doc_model': 'res.partner',
                'docs': docs,
                'get_lines': self.get_lines,
                'set_ageing': self.set_ageing,
                'set_amount': self.set_amount,
                'get_company_info': self.get_company_info,
            }
