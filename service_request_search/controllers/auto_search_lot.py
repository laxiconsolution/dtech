# -*- coding: utf-8 -*-

from odoo import http
from odoo.http import request
import json
import logging

_logger = logging.getLogger(__name__)


class WebsiteSearch(http.Controller):
    @http.route('/service/search', csrf=False, auth='public', website=True, type='http', methods=['GET'])
    def suggest_search(self, keywords, **params):
        if not keywords:
            return json.dumps([])

        Category = request.env['sale.order.line'].sudo().search([])
        if 'category' in params and params['category']:
            categories = Category.search([('lot_id.name', 'ilike', keywords)])
            categories = [dict(code=c.lot_id.name, type='c') for c in categories]
        return json.dumps(categories)
