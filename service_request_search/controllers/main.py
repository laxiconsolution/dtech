# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import http, _
from odoo.http import request
from odoo.addons.portal.controllers.portal import CustomerPortal, pager as portal_pager
from odoo.osv.expression import OR
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)


class CustomerPortal(CustomerPortal):

    def _prepare_portal_layout_values(self):
        values = super(CustomerPortal, self)._prepare_portal_layout_values()
        values['service_count'] = request.env['sale.order.line'].search_count([])
        return values

    def _lot_get_page_view_values(self, service, access_token, **kwargs):
        values = {
            'page_name': 'service',
            'service': service,
        }
        return self._get_page_view_values(service, access_token, values, 'my_service_history', False, **kwargs)

    @http.route(['/service', '/service/page/<int:page>'], type='http', auth="public", website=True)
    def my_service_search(self, page=1, date_begin=None, date_end=None, sortby=None, search=None, search_in='content', **kw):
        values = self._prepare_portal_layout_values()
        _items_per_page = 1
        name = ""
        if http.request.env.user.name != "Public user":
            name = http.request.env.user.name

        customer = http.request.env.user.partner_id.name
        email = http.request.env.user.partner_id.email
        phone = http.request.env.user.partner_id.phone
        domain = []

        searchbar_inputs = {
            'all': {'input': 'all', 'label': _('Search in All')},
        }

        archive_groups = self._get_archive_groups('sale.order.line', domain)
        if date_begin and date_end:
            domain += [('create_date', '>', date_begin), ('create_date', '<=', date_end)]

        if search:
            search_domain = []
            search_domain = OR([search_domain, [('lot_id.name', 'ilike', search)]])
            domain += search_domain

        # pager
        services_count = request.env['sale.order.line'].search_count(domain)
        pager = portal_pager(
            url="/service",
            url_args={'date_begin': date_begin, 'date_end': date_end},
            total=services_count,
            page=page,
            step=self._items_per_page
        )

        services = request.env['sale.order.line'].search(domain, limit=_items_per_page, offset=pager['offset'])
        request.session['my_service_history'] = services.ids[:1]

        values.update({
            'date': date_begin,
            'services': services,
            'page_name': 'service',
            'default_url': '/service',
            'pager': pager,
            'archive_groups': archive_groups,
            'searchbar_inputs': searchbar_inputs,
            'search_in': search_in,
            'search': search,
            'partner_id': customer,
            'user_ids': name,
            'email': email,
            'phone': phone,
        })
        return request.render("service_request_search.my_service_search", values)

    @http.route('/service/thanks', type="http", auth="public", csrf=False, website=True)
    def my_service_request_thanks(self, **post):

        if post.get('debug'):
            return request.render("service_request_search.service_request_thank_you")
        if post:
            if post['support_team_id'] == '':
                support_team_obj = False
                user_id = False
                team_leader_id = False
            else:
                support_team_obj = request.env['support.team'].sudo().browse(int(post['support_team_id']))
                user_id = support_team_obj.user_id.id
                team_leader_id = support_team_obj.user_id.id

            name = post['name']
            description = post['description']
            email_from = post['email_from']
            phone = post['phone']
            partner_id = post['partner_id']
            sale_order_id = post['sale_order_id']
            category = post['ticket_id']
            priority = post['priority']
            date_create = datetime.now()
            support_team_id = post['support_team_id']
            sale_order = request.env['sale.order'].search([('name', '=', sale_order_id)])
            partner_brw = request.env['res.partner'].search([('name', '=', partner_id)])

            vals = {
                'name': name,
                'description': description,
                'email_from': email_from,
                'phone': phone,
                'category': category,
                'priority': priority,
                'sale_order_id': sale_order.id,
                'partner_id': partner_brw.id,
                'state': 'draft',
                'date_create': date_create,
                'support_team_id': support_team_id or False,
                'user_id': user_id or False,
                'team_leader_id': team_leader_id or False,
            }

            support_ticket_id = request.env['service.request'].sudo().create(vals)
            print("===----------", support_ticket_id.name)
            template_id = request.env['ir.model.data'].get_object_reference('service_request_search', 'email_template_service_request')[1]
            print("---Templateeeeeeeeeee", template_id)
            email_template_obj = request.env['mail.template'].sudo().browse(template_id)
            if template_id:
                values = email_template_obj.generate_email(support_ticket_id.id)
                print("-------values--------", values)
                values['email_from'] = support_ticket_id.partner_id.email
                if email_from:
                    values['email_to'] = email_from
                values['author_id'] = support_ticket_id.partner_id.id
                values['res_id'] = False
                mail_mail_obj = request.env['mail.mail']
                print("-----mail-----mailllllll", mail_mail_obj)
                print("-----emailllllllllllllllllll", email_from)
                msg_id = mail_mail_obj.sudo().create(values)
                print("-----mail-----iddddddd", msg_id)
                if msg_id:
                    mail_mail_obj.send([msg_id])

            return request.render("service_request_search.service_request_thank_you")
