# -*- encoding: utf-8 -*-

{
    'name': 'Service Request Search',
    "version": "12.0.1.0.0",
    "summary": "Website",
    'description': """
        Service Request Search
    """,
    'category': 'Product Website',
    'sequence': 10,
    'author': "Laxicon Solution",
    'website': 'https://www.Laxicon.in',
    'depends': ['website', 'sale_management'],
    'data': [
        'security/ir.model.access.csv',
        'security/service_request_security.xml',
        'views/menu_service_request.xml',
        'views/template.xml',
        'views/service_portal_template.xml',
        'views/service_request.xml',
        'views/support_team.xml',
        'views/category_type.xml',
        'data/service_request_mail_data.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
